# Warning: A deprecated function has been called.
# USAGE: warning_deprecated_function $old_function $new_function
warning_deprecated_function() {
	local old_function new_function
	old_function="$1"
	new_function="$2"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La fonction suivante est dépréciée : %s\n'
			message="$message"'Cette nouvelle fonction devrait être utilisée à sa place : %s\n\n'
		;;
		('en'|*)
			message='The following function is deprecated: %s\n'
			message="$message"'This new function should be used instead: %s\n\n'
		;;
	esac

	# Print the message on the standard error output,
	# to avoid messing up the regular output of the function that triggered this warning.
	print_message 'warning_once' "$message" \
		"$old_function" \
		"$new_function" \
		> /dev/stderr
}

# Warning: A deprecated variable is set.
# USAGE: warning_deprecated_variable $old_variable $new_variable
warning_deprecated_variable() {
	local old_variable new_variable
	old_variable="$1"
	new_variable="$2"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La variable suivante est dépréciée : %s\n'
			message="$message"'Cette nouvelle variable devrait être utilisée à sa place : %s\n\n'
		;;
		('en'|*)
			message='The following variable is deprecated: %s\n'
			message="$message"'This new variable should be used instead: %s\n\n'
		;;
	esac

	# Print the message on the standard error output,
	# to avoid messing up the regular output of the variable that triggered this warning.
	print_message 'warning_once' "$message" \
		"$old_variable" \
		"$new_variable" \
		> /dev/stderr
}

# Warning: An option is set to a deprecated value.
# USAGE: warning_option_value_deprecated $option_name $option_value
warning_option_value_deprecated() {
	local option_name option_value
	option_name="$1"
	option_value="$2"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La valeur suivante est dépréciée pour lʼoption "%s", et ne sera plus acceptée dans une future version : "%s"\n\n'
		;;
		('en'|*)
			message='The following value is deprecated for option "%s", and will no longer be supported with some future update: "%s"\n\n'
		;;
	esac

	# Print the message on the standard error output,
	# to avoid messing up the regular output of the variable that triggered this warning.
	print_message 'warning_once' "$message" \
		"$option_name" \
		"$option_value" \
		> /dev/stderr
}

# Warning: An archive has a deprecated type set.
# USAGE: warning_archive_type_deprecated $archive
warning_archive_type_deprecated() {
	local archive
	archive="$1"

	local archive_type game_name
	archive_type=$(archive_type "$archive")
	game_name=$(game_name)

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La prise en charge de "%s" utilise un type dʼarchive déprécié : %s\n\n'
		;;
		('en'|*)
			message='Support for "%s" is using an obsolete archive type: %s\n\n'
		;;
	esac

	# Print the message on the standard error output,
	# to avoid messing up the regular output of the variable that triggered this warning.
	print_message 'warning_once' "$message" \
		"$game_name" \
		"$archive_type" \
		> /dev/stderr
}

# Warning - The legacy variable has been use to set the current archive.
# USAGE: warning_context_legacy_archive
warning_context_legacy_archive() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Lʼarchive actuelle a été définie en utilisant la variable dépréciée $ARCHIVE.\n'
			message="$message"'La fonction set_current_archive devrait être utilisée à sa place.\n'
		;;
		('en'|*)
			message='The current archive has been set using the deprecated variable $ARCHIVE.\n'
			message="$message"'The function set_current_archive should be used instead.\n'
		;;
	esac

	# Print the message on the standard error output,
	# to avoid messing up the regular output of the function that triggered this warning.
	print_message 'warning_once' "$message" > /dev/stderr
}

# Warning - The legacy variable has been use to set the current package.
# USAGE: warning_context_legacy_package
warning_context_legacy_package() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le paquet actuel a été défini en utilisant la variable dépréciée $PKG.\n'
			message="$message"'La fonction set_current_package devrait être utilisée à sa place.\n'
		;;
		('en'|*)
			message='The current package has been set using the deprecated variable $PKG.\n'
			message="$message"'The function set_current_package should be used instead.\n'
		;;
	esac

	# Print the message on the standard error output,
	# to avoid messing up the regular output of the function that triggered this warning.
	print_message 'warning_once' "$message" > /dev/stderr
}

