# DOSBox - Print the DOSBox pre-run actions for the given application.
# These actions are run inside DOSBox.
# USAGE: dosbox_prerun $application
# RETURN: the pre-run actions, can span over multiple lines,
#         or an empty string if there are none
dosbox_prerun() {
	local application
	application="$1"

	get_value "${application}_DOSBOX_PRERUN"
}

# DOSBox - Print the DOSBox post-run actions for the given application.
# These actions are run inside DOSBox.
# USAGE: dosbox_postrun $application
# RETURN: the post-run actions, can span over multiple lines,
#         or an empty string if there are none
dosbox_postrun() {
	local application
	application="$1"

	get_value "${application}_DOSBOX_POSTRUN"
}

