# WINE - Print the list of winetricks verbs that should be applied during the WINE prefix initialization.
# USAGE: wine_winetricks_verbs
# RETURN: A list of winetricks verbs,
#         the list can be empty.
wine_winetricks_verbs() {
	local winetricks_verbs
	winetricks_verbs=$(context_value 'WINE_WINETRICKS_VERBS')

	# Fall back on the default list of winetricks verbs for the current game engine
	if [ -z "$winetricks_verbs" ]; then
		local game_engine
		game_engine=$(game_engine)
		case "$game_engine" in
			('unrealengine4')
				winetricks_verbs=$(unrealengine4_wine_winetricks_verbs_default)
			;;
		esac
	fi

	# Append the verb used to set a virtual desktop, if required
	local virtual_desktop
	virtual_desktop=$(wine_virtual_desktop)
	case "$virtual_desktop" in
		('none') ;;
		('auto')
			winetricks_verbs="$winetricks_verbs vd=\"\$(screen_resolution)\""
		;;
		(*)
			winetricks_verbs="$winetricks_verbs vd=$virtual_desktop"
		;;
	esac

	printf '%s' "$winetricks_verbs"
}

# WINE - Setup the winetricks wrapper
# USAGE: wine_winetricks_wrapper
wine_winetricks_wrapper() {
	cat <<- 'EOF'
	## The `wineserver` command is only used by winetricks
	wineserver_command() {
	    wine_command | sed 's#/wine$#/wineserver#'
	}
	## Apply winetricks verbs, spawning a terminal if required
	winetricks_wrapper() {
	    ## Export custom paths to WINE commands
	    ## so winetricks use them instead of the default paths
	    WINE=$(wine_command)
	    WINESERVER=$(wineserver_command)
	    WINEBOOT=$(wineboot_command)
	    export WINE WINESERVER WINEBOOT

	    ## Run winetricks, spawning a terminal if required
	    ## to ensure it is not silently running in the background
	    if [ -t 0 ] || command -v zenity kdialog >/dev/null; then
	        winetricks "$@"
	    else
	        $(terminal_wrapper) -e \
	            env \
	            WINE="$WINE" \
	            WINESERVER="$WINESERVER" \
	            WINEBOOT="$WINEBOOT" \
	            WINEARCH="$WINEARCH" \
	            WINEDEBUG="$WINEDEBUG" \
	            WINEPREFIX="$WINEPREFIX" \
	            winetricks "$@"
	    fi

	    ## Wait a bit for lingering WINE processes to terminate
	    sleep 1s
	}

	EOF
}

