# WINE - List the tweaks that should be applied to the WINE prefix
# USAGE: wine_wineprefix_tweaks
# RETURN: the list of tweaks, one per line
wine_wineprefix_tweaks() {
	local tweaks_list
	tweaks_list=${WINE_WINEPREFIX_TWEAKS:-}

	printf '%s' "$tweaks_list" | list_clean
}

# WINE - Print the snippet installing Mono in the WINE prefix
# USAGE: wine_wineprefix_tweak_mono_install
# RETURN: the code snippet, for inclusion in the game launcher script
wine_wineprefix_tweak_mono_install() {
	local game_id mono_installer_name
	game_id=$(game_id)
	mono_installer_name=$(archive_name 'ARCHIVE_MONO')
	{
		cat <<- EOF
		    ## Install Mono in the WINE prefix.
		    \$(wine_command) "\${WINEPREFIX}/drive_c/${game_id}/wineprefix-tweaks/${mono_installer_name}"

		EOF
	}
}

