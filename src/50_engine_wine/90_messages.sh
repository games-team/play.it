# Error - The provided value for the WINE Direct3D renderer is invalid
# USAGE: error_unknown_wine_renderer $unknown_value
error_unknown_wine_renderer() {
	local unknown_value
	unknown_value="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La valeur suivante est invalide pour WINE_DIRECT3D_RENDERER : %s\n'
		;;
		('en'|*)
			message='The following value is not supported for WINE_DIRECT3D_RENDERER: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$unknown_value"
}

# Error - The provided setting for the WINE virtual desktop is invalid.
# USAGE: error_wine_virtual_desktop_invalid $invalid_setting
error_wine_virtual_desktop_invalid() {
	local invalid_setting
	invalid_setting="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La valeur suivante est invalide pour WINE_VIRTUAL_DESKTOP : "%s"\n'
		;;
		('en'|*)
			message='The following value is not supported for WINE_VIRTUAL_DESKTOP: "%s"\n'
		;;
	esac
	print_message 'error' "$message" \
		"$invalid_setting"
}

