# Set the extra paths that are used by WINE launchers
# USAGE: wine_init_paths
wine_init_paths() {
	# Set the path to the WINE prefix
	local game_id
	game_id=$(game_id)
	cat <<- EOF
	## Set the path to the WINE prefix
	path_wineprefix() {
	    # The prefix path can be overriden using an environment variable
	    if [ -n "\${WINEPREFIX:-}" ]; then
	        printf '%s' "\$WINEPREFIX"
	        return 0
	    fi
	    # Compute the default prefix path if none has been explicitely set
	    printf '%s/play.it/wine/%s' \
	        "\${XDG_CACHE_HOME:="\$HOME/.cache"}" \
	        "${game_id}"
	}
	PATH_WINEPREFIX=\$(path_wineprefix)

	EOF

	# Set the path to store registry dumps
	if [ -n "${WINE_REGEDIT_PERSISTENT_KEYS:-}" ]; then
		cat <<- 'EOF'
		## Set the path to store registry dumps
		path_wine_registry() {
		    printf '%s/wine/regedit' "$PATH_PERSISTENT"
		}
		PATH_WINE_REGISTRY=$(path_wine_registry)

		EOF
	fi
}

