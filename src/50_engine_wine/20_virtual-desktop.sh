# WINE - Get the WINE virtual desktop setting expected by the current game.
#
# The value should be one of:
# - none (default if no value is set)
# - auto (use the current screen resolution when the game is launched for the first time)
# - some specific resolution (example: "1280x1024")
#
# USAGE: wine_virtual_desktop
wine_virtual_desktop() {
	local virtual_desktop
	virtual_desktop=${WINE_VIRTUAL_DESKTOP:-none}

	case "$virtual_desktop" in
		('none'|'auto') ;;
		(*[0-9]x[0-9]*)
			if ! printf '%s' "$virtual_desktop" | grep --silent '^[0-9]\+x[0-9]\+$'; then
				error_wine_virtual_desktop_invalid "$virtual_desktop"
			fi
		;;
		(*)
			error_wine_virtual_desktop_invalid "$virtual_desktop"
		;;
	esac

	printf '%s' "$virtual_desktop"
}

# WINE - Get the display resolution of the current screen.
# USAGE: wine_snippet_screen_resolution
wine_snippet_screen_resolution() {
	cat <<- 'EOF'
	# Return the type of the current display server.
	# Supported return values:
	# - wayland/sway
	# - xorg
	# If no supported server is running, nothing is returned.
	display_server() {
	    if [ -n "${WAYLAND_DISPLAY:-}" ]; then
	        if [ -n "${SWAYSOCK:-}" ]; then
	            display_server='wayland/sway'
	        fi
	    elif [ -n "${DISPLAY:-}" ]; then
	        display_server='xorg'
	    fi
	    printf '%s' "${display_server:-}"
	    unset display_server
	}

	# Return the resolution of the current screen.
	# If no supported server is running, a fallback value is returned.
	screen_resolution() {
	    display_server=$(display_server)
	    case "$display_server" in
	        ('wayland/sway')
	            current_screen=$(
	                LANG=C swaymsg --pretty --type get_workspaces |
	                    grep --after 1 '(focused)' |
	                    sed --silent --expression='s/\s*Output: \(.*\)$/\1/p'
	            )
	            screen_resolution=$(
	                LANG=C swaymsg --pretty --type get_outputs |
	                    grep --after 1 "$current_screen" |
	                    sed --silent --expression='s/.*Current mode: \([0-9]\+x[0-9]\+\) @.*$/\1/p'
	            )
	            unset current_screen
	        ;;
	        ('xorg')
	            screen_resolution=$(
	                LANG=C xrandr |
	                    sed --regexp-extended --silent --expression='s/.*primary.* ([0-9]+x[0-9]+).*/\1/p'
	            )
	        ;;
	        (*)
	            ## Fall back on a default resolution if an unsupported display server is in use.
	            screen_resolution='1024x768'
	        ;;
	    esac
	    printf '%s' "${screen_resolution:-}"
	    unset display_server screen_resolution
	}
	EOF
}

