# WINE launcher - Set environment for registry keys persistent storage
# USAGE: wine_launcher_regedit_environment
wine_launcher_regedit_environment() {
	# Return early if no persistent registry keys are listed
	if [ -z "${WINE_REGEDIT_PERSISTENT_KEYS:-}" ]; then
		return 0
	fi

	local game_id
	game_id=$(game_id)
	cat <<- EOF
	# Set environment for registry keys persistent storage

	REGEDIT_DUMPS_WINEPREFIX_PATH="\${WINEPREFIX}/drive_c/${game_id}/wine/regedit"
	REGEDIT_PERSISTENT_KEYS='$WINE_REGEDIT_PERSISTENT_KEYS'

	EOF
	cat <<- 'EOF'
	## Convert registry key name to file path
	regedit_convert_key_to_path() {
	    printf '%s.reg' "$1" |
	        sed 's#\\#/#g' |
	        tr '[:upper:]' '[:lower:]'
	}

	EOF
}

# WINE launcher - Load registry keys during prefix initialization
# USAGE: regedit_initial
regedit_initial() {
	# Return early if there is no key to load during prefix initilization
	if [ -z "${APP_REGEDIT:-}" ]; then
		return 0
	fi

	local game_id
	game_id=$(game_id)
	cat <<- EOF
	    ## Load registry scripts
	    registry_scripts='$APP_REGEDIT'
	    (
	        cd "\${WINEPREFIX}/drive_c/${game_id}"
	EOF
	cat <<- 'EOF'
	        for registry_script in $registry_scripts; do
	            printf 'Loading registry script: %s\n' "$registry_script"
	            if [ ! -e "$registry_script" ]; then
	                printf '\n\033[1;31mError:\033[0m\n'
	                printf 'Failed to load required registry script: %s\n' "$registry_script"
	                exit 1
	            fi
	            set +o errexit
	            $(regedit_command) "$registry_script"
	            regedit_return_code=$?
	            set -o errexit
	            if [ $regedit_return_code -ne 0 ]; then
	                printf '\n\033[1;31mError:\033[0m\n'
	                printf 'Failed to load required registry script: %s\n' "$registry_script"
	                exit 1
	            fi
	            unset regedit_return_code
	        done
	        unset registry_script
	    )
	    unset registry_scripts

	EOF
}

# WINE launcher - Store registry keys in a persistent path
# USAGE: wine_launcher_regedit_store
wine_launcher_regedit_store() {
	# Return early if no persistent registry keys are listed
	if [ -z "${WINE_REGEDIT_PERSISTENT_KEYS:-}" ]; then
		return 0
	fi

	cat <<- 'EOF'
	# Store registry keys in a persistent path

	while read -r registry_key; do
	    if [ -z "$registry_key" ]; then
	        continue
	    fi
	    registry_dump="${REGEDIT_DUMPS_WINEPREFIX_PATH}/$(regedit_convert_key_to_path "$registry_key")"
	    registry_dump_directory=$(dirname "$registry_dump")
	    mkdir --parents "$registry_dump_directory"
	    printf 'Dumping registry key in "%s".\n' "$registry_dump"
	    $(regedit_command) -E "$registry_dump" "$registry_key"
	done << EOL
	$(printf '%s' "$REGEDIT_PERSISTENT_KEYS")
	EOL
	unset registry_key registry_dump registry_dump_directory

	mkdir --parents "$PATH_WINE_REGISTRY"
	(
	    cd "$REGEDIT_DUMPS_WINEPREFIX_PATH"
	    find . -type f \
	        -exec cp --force --parents --target-directory="$PATH_WINE_REGISTRY" {} +
	)

	EOF
}

# WINE launcher - Load registry keys from persistent dumps
# USAGE: wine_launcher_regedit_load
wine_launcher_regedit_load() {
	# Return early if no persistent registry keys are listed
	if [ -z "${WINE_REGEDIT_PERSISTENT_KEYS:-}" ]; then
		return 0
	fi

	cat <<- 'EOF'
	# Load registry keys from persistent dumps

	if [ -e "$PATH_WINE_REGISTRY" ]; then
	    mkdir --parents "$REGEDIT_DUMPS_WINEPREFIX_PATH"
	    (
	        cd "$PATH_WINE_REGISTRY"
	        find . -type f \
	            -exec cp --force --parents --target-directory="$REGEDIT_DUMPS_WINEPREFIX_PATH" {} +
	    )
	fi
	while read -r registry_key; do
	    if [ -z "$registry_key" ]; then
	        continue
	    fi
	    registry_dump="${REGEDIT_DUMPS_WINEPREFIX_PATH}/$(regedit_convert_key_to_path "$registry_key")"
	    if [ -e "$registry_dump" ]; then
	        printf 'Loading registry key from "%s".\n' "$registry_dump"
	        $(regedit_command) "$registry_dump"
	    fi
	done << EOL
	$(printf '%s' "$REGEDIT_PERSISTENT_KEYS")
	EOL
	unset registry_key registry_dump

	EOF
}

