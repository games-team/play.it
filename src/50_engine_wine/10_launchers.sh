# WINE launcher - Print the script content
# USAGE: wine_launcher $application
wine_launcher() {
	local application
	application="$1"

	local prefix_type
	prefix_type=$(application_prefix_type "$application")
	case "$prefix_type" in
		('symlinks')
			launcher_headers

			# Set the paths that should be available to the generated launcher
			launcher_init_paths "$application"

			wine_launcher_environment "$application"

			# Generate the game prefix
			prefix_generate_links_farm
			launcher_prefix_symlinks_build

			# Set up the paths diversion to persistent storage
			persistent_storage_initialization
			persistent_storage_common
			persistent_path_diversion
			persistent_storage_update_directories
			persistent_storage_update_files

			# Generate the WINE prefix
			wine_launcher_wineprefix_environment
			wine_launcher_wineprefix_generate
			wine_launcher_wineprefix_persistent

			# Handle persistent storage of registry keys
			wine_launcher_regedit_environment
			wine_launcher_regedit_load

			wine_launcher_run "$application"

			# Handle persistent storage of registry keys
			wine_launcher_regedit_store

			# Update persistent storage with files from the current prefix
			persistent_storage_update_files_from_prefix

			launcher_exit
		;;
		(*)
			error_launchers_prefix_type_unsupported "$application"
			return 1
		;;
	esac
}

# WINE launcher - Set the environment
# USAGE: wine_launcher_environment $application
wine_launcher_environment() {
	local application
	application="$1"

	local application_exe
	application_exe=$(application_exe_escaped "$application")

	cat <<- EOF
	# Set the environment

	APP_EXE='$application_exe'

	EOF
	cat <<- 'EOF'
	## Print the path to the `wine` command
	wine_command() {
	    if [ -z "$PLAYIT_WINE_CMD" ]; then
	        command -v wine
	        return 0
	    fi
	    printf '%s' "$PLAYIT_WINE_CMD"
	}
	wineboot_command() {
	    wine_command | sed 's#/wine$#/wineboot#'
	}

	EOF

	# Include the path to the `regedit` command only if it is going to be used
	if
		[ -n "${APP_REGEDIT:-}" ] ||
		[ -n "${WINE_REGEDIT_PERSISTENT_KEYS:-}" ]
	then
		cat <<- 'EOF'
		regedit_command() {
		    wine_command | sed 's#/wine$#/regedit#'
		}

		EOF
	fi

	# Include the terminal wrapper if winetricks is relied upon,
	# or if DXVK is going to be used for Direct3D rendering
	local winetricks_verbs
	winetricks_verbs=$(wine_winetricks_verbs)
	if
		[ -n "$winetricks_verbs" ] ||
		[ -n "${WINE_DIRECT3D_RENDERER:-}" ]
	then
		launcher_wrapper_terminal
	fi

	# Include the winetricks wrapper function only if it is going to be used
	if
		[ -n "$winetricks_verbs" ] ||
		[ -n "${WINE_DIRECT3D_RENDERER:-}" ]
	then
		wine_winetricks_wrapper
	fi

	# Include the screen resolution detection function if it is required to set a virtual desktop.
	local virtual_desktop
	virtual_desktop=$(wine_virtual_desktop)
	if [ "$virtual_desktop" = 'auto' ]; then
		wine_snippet_screen_resolution
	fi
}

# WINE - Print the snippet handling the actual run of the game
# USAGE: wine_launcher_run $application
wine_launcher_run() {
	local application
	application="$1"

	local game_id
	game_id=$(game_id)
	cat <<- EOF
	# Run the game

	cd "\${WINEPREFIX}/drive_c/${game_id}"

	EOF

	local game_engine
	game_engine=$(game_engine)
	case "$game_engine" in
		('visionaire')
			# Prevent the use of wayland SDL video driver
			visionaire_tweak_sdl_wine
		;;
	esac

	application_prerun "$application"

	cat <<- 'EOF'

	## Do not exit on application failure,
	## to ensure post-run commands are run.
	set +o errexit

	EOF
	game_exec_line "$application"
	cat <<- 'EOF'

	game_exit_status=$?
	set -o errexit

	EOF

	application_postrun "$application"
}

# WINE - Print the line starting the game
# USAGE: wine_exec_line $application
# RETURN: the command to execute, including its command line options
wine_exec_line() {
	local application
	application="$1"

	local application_options
	application_options=$(application_options "$application")
	cat <<- EOF
	\$(wine_command) "\$APP_EXE" $application_options "\$@"
	EOF
}

