# Error - The "custom_launcher" function has not been overriden by the game script
# USAGE: error_custom_launcher_not_set $application
error_custom_launcher_not_set() {
	local application
	application="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='L’application suivante nécessite que la fonction "custom_launcher" soit définie dans le script : %s\n'
		;;
		('en'|*)
			message='The following application expects the "custom_launcher" to be set by the game script: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$application"
}

