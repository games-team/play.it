# Custom launcher - Throw an error if not overriden from the game script
# USAGE: custom_launcher $application
custom_launcher() {
	local application
	application="$1"

	error_custom_launcher_not_set "$application"
	return 1
}

