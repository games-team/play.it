# Error - Unity3D - No binary has been found
# USAGE: error_unity3d_binary_not_found
error_unity3d_binary_not_found() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Aucun exécutable nʼa été trouvé pour ce jeu Unity3D.\n'
			message="$message"'Cette erreur est probablement due à une valeur incorrecte assignée à CONTENT_PATH_DEFAULT.\n'
		;;
		('en'|*)
			message='No binary has been found for the current Unity3D game.\n'
			message="$message"'This is probably due to an incorrect value for the CONTENT_PATH_DEFAULT variable.\n'
		;;
	esac
	print_message 'error' "$message"
}

