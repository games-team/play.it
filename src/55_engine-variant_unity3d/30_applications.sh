# Compute the file name of the game binary from the UNITY3D_NAME value
# USAGE: unity3d_application_exe_default application
# RETURN: the application file name,
#         or an empty string
unity3d_application_exe_default() {
	local application
	application="$1"

	# We can not rely on the application type here,
	# to avoid a loop between application_exe and application_type.
	local unity3d_name
	unity3d_name=$(unity3d_name)
	local package package_architecture
	package=$(current_package)
	package_architecture=$(package_architecture "$package")
	local filename_candidates_list filename_candidate filename_path filename_found
	case "$package_architecture" in
		('32')
			filename_candidates_list="
			${unity3d_name}.x86
			${unity3d_name}.exe
			${unity3d_name}"
		;;
		('64')
			filename_candidates_list="
			${unity3d_name}.x86_64
			${unity3d_name}.exe
			${unity3d_name}"
		;;
		(*)
			filename_candidates_list="
			${unity3d_name}.x86
			${unity3d_name}.x86_64
			${unity3d_name}.exe
			${unity3d_name}"
		;;
	esac
	## Use a while loop to avoid breaking on spaces in file name.
	while read -r filename_candidate; do
		## Skip empty lines.
		if [ -z "$filename_candidate" ]; then
			continue
		fi
		## Do not throw an error if no path is found for the current candidate.
		filename_path=$(application_exe_path "$filename_candidate") 2>/dev/null || true
		if [ -n "$filename_path" ]; then
			filename_found="$filename_candidate"
			break
		fi
	done <<- EOL
	$(printf '%s' "$filename_candidates_list")
	EOL

	## Throw an error if no binary has been found,
	## as this would cause other problems later in the script execution.
	if [ -z "${filename_found:-}" ]; then
		error_unity3d_binary_not_found
		return 1
	fi

	printf '%s' "$filename_found"
}

