# Print the WINE prefix initialization actions specific to Unity3D games
# USAGE: wine_wineprefix_init_actions_unity3d
wine_wineprefix_init_actions_unity3d() {
	cat <<- 'EOF'
	    ## Work around a loss of input on loss of focus with Unity3D games
	    ## cf. https://bugs.winehq.org/show_bug.cgi?id=48121
	    $(wine_command) reg add 'HKEY_CURRENT_USER\Software\Wine\X11 Driver' /t REG_SZ /v UseTakeFocus /d N /f

	EOF
}

