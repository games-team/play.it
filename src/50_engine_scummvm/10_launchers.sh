# ScummVM launcher - Print the script content
# USAGE: scummvm_launcher $application
scummvm_launcher() {
	local application
	application="$1"

	local prefix_type
	prefix_type=$(application_prefix_type "$application")
	case "$prefix_type" in
		('none')
			launcher_headers

			# Set the paths that should be available to the generated launcher
			launcher_init_paths "$application"

			scummvm_launcher_environment "$application"
			scummvm_launcher_run
			launcher_exit
		;;
		(*)
			error_launchers_prefix_type_unsupported "$application"
			return 1
		;;
	esac
}

# ScummVM launcher - Set the environment
# USAGE: scummvm_launcher_environment $application
scummvm_launcher_environment() {
	local application
	application="$1"

	local application_scummid
	application_scummid=$(application_scummvm_scummid "$application")

	cat <<- EOF
	# Set the environment

	SCUMMVM_ID='$application_scummid'

	EOF
}

# ScummVM launcher - Run ScummVM
# USAGE: scummvm_launcher_run
scummvm_launcher_run() {
	cat <<- 'EOF'
	# Run the game

	EOF

	application_prerun "$application"
	game_exec_line "$application"
	application_postrun "$application"
}

# ScummVM - Print the line starting the game
# USAGE: scummvm_exec_line $application
# RETURN: the command to execute, including its command line options
scummvm_exec_line() {
	local application
	application="$1"

	local application_options
	application_options=$(application_options "$application")
	cat <<- EOF
	scummvm --path="\$PATH_GAME_DATA" $application_options "\$@" "\$SCUMMVM_ID"
	EOF
}

