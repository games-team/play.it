# Check if the current process is running under the root account
# USAGE: check_is_running_as_root
# RETURN: 0 if running as root, 1 otherwise
check_is_running_as_root() {
	local current_user_id
	current_user_id=$(id --user)

	test "$current_user_id" -eq 0
}

# Throw an error is the current process has been called by the root account
# USAGE: init_fail_as_root
init_fail_as_root() {
	## This check can be skipped by setting the following environment variable:
	## PLAYIT_OPTION_RUN_AS_ROOT=1
	if
		[ "${PLAYIT_OPTION_RUN_AS_ROOT:-0}" -eq 0 ] &&
		check_is_running_as_root
	then
		error_run_as_root
		exit 1
	fi
}

