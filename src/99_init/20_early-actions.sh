# If some early action is set, run it then exit
# The following switches trigger an early action:
# - --help
# - --version
# - --list-available-scripts
# - --list-supported-games
# USAGE: init_early_actions
init_early_actions() {
	init_help
	init_version
	init_scripts_list
	init_games_list
}

# Display the list of available game scripts then exit, if called with --list-available-scripts
# USAGE: init_scripts_list
init_scripts_list() {
	local option_list_available_scripts
	option_list_available_scripts=$(option_value 'list-available-scripts')
	if [ "${option_list_available_scripts:-0}" -eq 0 ]; then
		return 0
	fi

	games_list_scripts_all

	exit 0
}

# List supported games then exit, if called with --list-supported-games
# USAGE: init_games_list
init_games_list() {
	## Return early if ./play.it has not been called in games listing mode
	local option_list_supported_games
	option_list_supported_games=$(option_value 'list-supported-games')
	## If called before the options parsing step, the previous function might have returned an empty string.
	if [ "${option_list_supported_games:-0}" -eq 0 ]; then
		return 0
	fi

	## List the games supported by the current game script, or all games if called from the "play.it" wrapper.
	local script_name
	script_name=$(basename "$0")
	if [ "$script_name" = 'play.it' ]; then
		games_list_supported_all
	else
		games_list_supported
	fi

	## Exit after printing the games listing, a no further action should be taken
	exit 0
}

# Display the help message then exit, if called with --help
# USAGE: init_help
init_help() {
	local option_help
	option_help=$(option_value 'help')
	if [ "$option_help" -eq 1 ]; then
		help
		exit 0
	fi
}

# Display the version string then exit, if called with --version
# USAGE: init_version
init_version() {
	local option_version
	option_version=$(option_value 'version')
	if [ "$option_version" -eq 1 ]; then
		printf '%s\n' "$LIBRARY_VERSION"
		exit 0
	fi
}

# If some no-op action is set, run it then exit
# The following switches trigger a no-op action:
# - --list-packages
# - --list-requirements
# - --show-game-script
# USAGE: init_noop_actions
init_noop_actions() {
	init_list_packages
	init_list_requirements
	init_show_game_script
}

# Print the list of packages that would be generated from the current archive then exit, if called with --list-packages
# USAGE: init_list_packages
init_list_packages() {
	local option_list_packages
	option_list_packages=$(option_value 'list-packages')
	if [ "$option_list_packages" -eq 0 ]; then
		return 0
	fi

	local archive
	archive=$(current_archive)
	packages_print_list "$archive"

	# Delete temporary files
	working_directory_cleanup

	exit 0
}

# Print the list of commands required to run the current game script then exit, if called with --list-requirements
# USAGE: init_list_requirements
init_list_requirements() {
	local option_list_requirements
	option_list_requirements=$(option_value 'list-requirements')
	if [ "$option_list_requirements" -eq 0 ]; then
		return 0
	fi

	requirements_list

	# Delete temporary files
	working_directory_cleanup

	exit 0
}

# Display the path to the game script then exit, if called with --show-game-script
# USAGE: init_show_game_script
init_show_game_script() {
	local option_show_game_script
	option_show_game_script=$(option_value 'show-game-script')
	if [ "$option_show_game_script" -eq 0 ]; then
		return 0
	fi

	realpath "$0"

	# Delete temporary files
	working_directory_cleanup

	exit 0
}

# Exit early if all packages are already built
# USAGE: init_packages_already_built
init_packages_already_built() {
	local option_overwrite
	option_overwrite=$(option_value 'overwrite')
	if [ "$option_overwrite" -eq 1 ]; then
		return 0
	fi

	local option_output_dir archive generated_packages_list
	option_output_dir=$(option_value 'output-dir')
	archive=$(current_archive)
	generated_packages_list=$(packages_print_list "$archive")
	while read -r generated_package; do
		if [ ! -e "${option_output_dir}/${generated_package}" ]; then
			return 0
		fi
	done <<- EOL
	$(printf '%s' "$generated_packages_list")
	EOL

	info_all_packages_already_built

	# Delete temporary files
	working_directory_cleanup

	exit 0
}

