# Set the options for the current shell
# USAGE: init_shell_options
init_shell_options() {
	# Exit immediately on error
	set -o errexit

	# Error out (and exit) when trying to expand an unset variable
	## Only for game scripts targeting ./play.it ≥ 2.23
	if compatibility_level_is_at_least '2.23'; then
		set -o nounset
	fi

	# Set input field separator to default value (space, tab, newline)
	unset IFS

	# Force umask to ensure all paths are created with correct permissions
	umask 0022
}

