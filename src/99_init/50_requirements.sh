# Check for the presence of required tools
# USAGE: init_requirements_check
init_requirements_check() {
	if ! check_deps; then
		# Delete temporary files
		working_directory_cleanup

		exit 1
	fi

	local archive
	archive=$(current_archive)
	if ! archive_dependencies_check "$archive"; then
		# Delete temporary files
		working_directory_cleanup

		exit 1
	fi
}

