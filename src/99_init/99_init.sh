# Run the common initialization process
# USAGE: initialization_default $command_line_argument[…]
initialization_default() {
	# Exit if the current process has been spawned by the root user
	init_fail_as_root

	# Set the options for the current shell
	init_shell_options

	# Check early if ./play.it has been called in games listing mode, since most of the initialization steps are not required in this mode.
	# This is called before options parsing because the games listing mode might have been set by a parent ./play.it process.
	init_games_list

	# Set/unset some environment variables
	init_environment

	# Check the expected compatibility level against the current library version
	init_compatibility_level_check

	# Set the default and current options
	init_options "$@"

	# If some early action is set, run it then exit
	init_early_actions

	# Check the validity of current options
	init_options_validity_check

	# Set the path to the temporary directory
	init_working_directory

	# Set up the base archive
	init_archive

	# Set the default package, used by the context system when no explicit package context is set
	init_package

	# If some no-op action is set, run it then exit
	## TODO: This could probably be run earlier, as soon as the base archive is set
	init_noop_actions

	# Exit early if all packages are already built
	init_packages_already_built

	# Check for the presence of required tools
	init_requirements_check

	# Check for the presence of extra required archives
	init_extra_archives_required

	# Check for the presence of extra optional archives
	init_extra_archives_optional

	# Check the archives integrity
	init_archives_integrity_check
}

# Automatically run the initialization process when sourcing the library from an old game script
# This can be bypassed by setting $LIB_ONLY to any non-null value
if \
	[ -z "${LIB_ONLY:-}" ] && \
	! compatibility_level_is_at_least '2.31'
then
	initialization_default "$@"
fi

