# Check the expected compatibility level against the current library version
# USAGE: init_compatibility_level_check
init_compatibility_level_check() {
	local compatibility_level compatibility_level_major library_version_major
	compatibility_level=$(compatibility_level)
	compatibility_level_major=$(printf '%s' "$compatibility_level" | cut --delimiter='.' --fields=1)
	library_version_major=$(printf '%s' "$LIBRARY_VERSION" | cut --delimiter='.' --fields=1)
	if [ "$library_version_major" -lt "$compatibility_level_major" ]; then
		error_incompatible_versions
		exit 1
	elif [ "$library_version_major" -eq "$compatibility_level_major" ]; then
		local compatibility_level_minor library_version_minor
		compatibility_level_minor=$(printf '%s' "$compatibility_level" | cut --delimiter='.' --fields=2)
		library_version_minor=$(printf '%s' "$LIBRARY_VERSION" | cut --delimiter='.' --fields=2)
		if [ "$library_version_minor" -lt "$compatibility_level_minor" ]; then
			error_incompatible_versions
			exit 1
		fi
	fi
}

