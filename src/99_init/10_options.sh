# Set the default and current options
# USAGE: init_options $command_line_option[…]
init_options() {
	# Set hardcoded defaults
	options_init_default

	# Load defaults from the configuration file
	local config_file_path
	config_file_path=$(find_configuration_file "$@")
	load_configuration_file "$config_file_path"

	# Set options from the command-line
	parse_arguments "$@"
}

# Check the validity of current options
# USAGE: init_options_validity_check
init_options_validity_check() {
	options_validity_check
	options_compatibility_check
}

