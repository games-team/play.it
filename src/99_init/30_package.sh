# Set the default package, used by the context system when no explicit package context is set
# USAGE: init_package
init_package() {
	local packages_list default_package
	packages_list=$(packages_list)
	default_package=$(printf '%s' "$packages_list" | head --lines=1)
	set_default_package "$default_package"
}

