# Set/unset some environment variables
# USAGE: init_environment
init_environment() {
	# Unset variables that we do not want to import from the user environment
	## TODO: This should be handled by the functions initializing these variables instead.
	unset SOURCE_ARCHIVE_PATH

	# Set URLs for error messages
	PLAYIT_GAMES_BUG_TRACKER_URL='https://forge.dotslashplay.it/play.it/games/issues'
	PLAYIT_BUG_TRACKER_URL='https://forge.dotslashplay.it/play.it/scripts/issues'
}

