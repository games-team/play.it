# Include the icons provided by an extra archive
# USAGE: content_inclusion_optional_icons_archive $package
content_inclusion_optional_icons_archive() {
	local package
	package="$1"

	# Proceed with the actual files inclusion
	local path_icons
	path_icons=$(path_icons)
	content_inclusion 'ICONS' "$package" "$path_icons"
}

