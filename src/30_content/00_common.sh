# Print the default path to the game data in the archive
# USAGE: content_path_default
# RETURN: a path relative to the archive root
content_path_default() {
	local content_path_default
	content_path_default=$(context_value 'CONTENT_PATH_DEFAULT')

	if [ -z "$content_path_default" ]; then
		error_missing_variable 'CONTENT_PATH_DEFAULT'
		return 1
	fi

	printf '%s' "$content_path_default"
}

# Print the path to the game data in the archive for a given identifier
# USAGE: content_path $content_id
# RETURN: a path relative to the archive root,
#         or an empty path if none is set
content_path() {
	## The content id is usually a concatenation of one of the following prefixes with a package suffix:
	## - DOC
	## - FONTS
	## - GAME
	## - LIBS
	## This is not a hard requirement, any arbitrary value can be used
	local content_id
	content_id="$1"


	# Use the path relative to the archive root if it is set
	local content_path
	content_path=$(context_value "CONTENT_${content_id}_PATH")

	# Use the path relative to $CONTENT_PATH_DEFAULT if it is set
	# It overrides the path relative to the archive root is this one is already set
	local content_path_relative
	content_path_relative=$(context_value "CONTENT_${content_id}_RELATIVE_PATH")
	if [ -n "$content_path_relative" ]; then
		local content_path_default
		content_path_default=$(content_path_default)
		content_path="${content_path_default}/${content_path_relative}"
	fi

	# Fall back on the default path for the current game engine
	if [ -z "$content_path" ]; then
		local game_engine
		game_engine=$(game_engine)
		case "$game_engine" in
			('visionaire')
				content_path=$(visionaire_content_path "$content_id")
			;;
		esac
	fi

	# Fall back on the default content path if no explicit value is set
	if [ -z "$content_path" ]; then
		## Do not fail if no default path is set, an empty value is returned instead
		## The output redirection must be done inside the subshell, or bash --posix will ignore it
		content_path=$(content_path_default 2>/dev/null) || true
	fi

	printf '%s' "$content_path"
}

# Print the list of files to include from the archive for a given identifier
# USAGE: content_files $content_id
# RETURN: a list of paths relative to the path for the given identifier,
#         line breaks are used as separator between each item,
#         this list can include globbing patterns,
#         this list can be empty
content_files() {
	local content_id
	content_id="$1"

	local content_files
	content_files=$(context_value "CONTENT_${content_id}_FILES")

	# Fall back on the default files list for the current game engine
	if [ -z "$content_files" ]; then
		local game_engine
		game_engine=$(game_engine)
		case "$game_engine" in
			('unity3d')
				content_files=$(unity3d_content_files_default "$content_id")
			;;
			('unrealengine4')
				content_files=$(unrealengine4_content_files_default "$content_id")
			;;
			('visionaire')
				content_files=$(visionaire_content_files "$content_id")
			;;
		esac
	fi

	printf '%s' "$content_files"
}

