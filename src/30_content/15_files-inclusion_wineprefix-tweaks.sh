# Include files required to apply tweaks to the WINE prefix
# USAGE: content_inclusion_wineprefix_tweaks $package
content_inclusion_wineprefix_tweaks() {
	local package
	package="$1"

	local wineprefix_tweaks wineprefix_tweak
	wineprefix_tweaks=$(wine_wineprefix_tweaks)
	while read -r wineprefix_tweak; do
		case "$wineprefix_tweak" in
			('mono')
				content_inclusion_wineprefix_tweaks_mono "$package"
			;;
		esac
	done <<- EOL
	$(printf '%s' "$wineprefix_tweaks")
	EOL
}

# Include the Mono .msi installer
# USAGE: content_inclusion_wineprefix_tweaks_mono $package
content_inclusion_wineprefix_tweaks_mono() {
	local package
	package="$1"

	local mono_installer_source package_path path_game_data mono_installer_name mono_installer_destination
	mono_installer_source=$(archive_path 'ARCHIVE_MONO')
	package_path=$(package_path "$package")
	path_game_data=$(
		set_current_package "$package"
		path_game_data
	)
	mono_installer_name=$(archive_name 'ARCHIVE_MONO')
	mono_installer_destination="${package_path}${path_game_data}/wineprefix-tweaks/${mono_installer_name}"
	install -D --mode=644 "$mono_installer_source" "$mono_installer_destination"
}

