# Information: Icons from the archives are included into packages paths
# USAGE: information_icons_inclusion
information_icons_inclusion() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Inclusion des icônes…\n'
		;;
		('en'|*)
			message='Including icons…\n'
		;;
	esac
	print_message 'info_once' "$message"
}

# Information: Files from the archives are included into packages paths
# USAGE: information_content_inclusion
information_content_inclusion() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Inclusion des fichiers du jeu…\n'
		;;
		('en'|*)
			message='Including game files…\n'
		;;
	esac
	print_message 'info_once' "$message"
}

# Information: A huge file is split into 9GB chunks
# USAGE: information_huge_file_split $path_file
information_huge_file_split() {
	local path_file
	path_file="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Découpage de %s en plusieurs fichiers…\n'
		;;
		('en'|*)
			message='Splitting %s into smaller chunks…\n'
		;;
	esac
	print_message 'info' "$message" \
		"$path_file"
}

