# Get the language set for messages in the current environment
# USAGE: messages_language
# RETURN: the language code, as a two-letters code, or the fallback value "C"
messages_language() {
	# Relying on the "locale" command prevents the need to query the values of multiple variables,
	# and handle the priority between them.
	locale | sed --silent 's/LC_MESSAGES="\?\([^_"]*\).*/\1/p'
}

