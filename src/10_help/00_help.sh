# display full usage instructions
# USAGE: help
help() {
	local script_name
	script_name=$(basename "$0")

	# print general usage instructions
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			if [ "$script_name" = 'play.it' ]; then
				message='\nUtilisation : %s ARCHIVE [OPTION]…\n\n'
			else
				message='\nUtilisation : %s [OPTION]… [ARCHIVE]\n\n'
			fi
		;;
		('en'|*)
			if [ "$script_name" = 'play.it' ]; then
				message='\nUsage: %s ARCHIVE [OPTION]…\n\n'
			else
				message='\nUsage: %s [OPTION]… [ARCHIVE]\n\n'
			fi
		;;
	esac
	print_message 'info' "$message" \
		"$script_name"

	# print details about options usage
	print_message 'info' '%s\n\n' \
		'OPTIONS'
	help_checksum
	help_compression
	help_prefix
	help_package
	help_icons
	help_overwrite
	help_output_dir
	help_no_mtree
	help_tmpdir
	help_skipfreespacecheck
	help_collection_path
	help_show_game_script
	help_configfile
	help_listpackages
	help_listrequirements
	help_listavailablescripts
	help_listsupportedgames

	# Do not print a list of supported archives if called throught the "play.it" wrapper script
	if [ "$script_name" = 'play.it' ]; then
		return 0
	fi

	# print list of supported archives
	print_message 'info' '%s\n\n' \
		'ARCHIVE'
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Ce script reconnaît les archives suivantes :\n'
		;;
		('en'|*)
			message='This script can work on the following archives:\n'
		;;
	esac
	print_message 'info' "$message"
	# shellcheck disable=SC2046
	information_archives_list $(archives_list)

	return 0
}

# display --checksum option usage
# USAGE: help_checksum
help_checksum() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tChoix de la méthode de vérification dʼintégrité de lʼarchive\n\n'
			message="$message"'\t%s\tvérification via md5sum\n' # md5
			message="$message"'\t%s\tpas de vérification\n\n'   # none
		;;
		('en'|*)
			message='\tArchive integrity verification method selection\n\n'
			message="$message"'\t%s\tmd5sum verification\n' # md5
			message="$message"'\t%s\tno verification\n\n'   # none
		;;
	esac
	print_message 'info' '--%s %s\n' \
		'checksum' \
		'md5|none'
	print_message 'info' "$message" \
		'md5' \
		'none'
}

# Display --compression option usage
# USAGE: help_compression
help_compression() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tChoix de la méthode de compression des paquets générés\n'
			message="$message"'\t%s\tpas de compression\n'
			message="$message"'\t%s\tméthode de compression mettant lʼaccent sur la rapidité\n'
			message="$message"'\t%s\tméthode de compression mettant lʼaccent sur la réduction de taille\n'
			message="$message"'\t%s\tméthode de compression par défaut du système actuel\n\n'
		;;
		('en'|*)
			message='\tGenerated packages compression method selection\n'
			message="$message"'\t%s\tno compression\n'
			message="$message"'\t%s\tcompression method focusing on compression speed\n'
			message="$message"'\t%s\tcompression method focusing on size reduction\n'
			message="$message"'\t%s\tdefault compression method on the current system\n\n'
		;;
	esac
	print_message 'info' '--%s %s\n' \
		'compression' \
		'none|speed|size|auto'
	print_message 'info' "$message" \
		'none' \
		'speed' \
		'size' \
		'auto'
}

# display --prefix option usage
# USAGE: help_prefix
help_prefix() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tChoix du chemin dʼinstallation du jeu\n\n'
			message="$message"'\tCette option accepte uniquement un chemin absolu.\n\n'
		;;
		('en'|*)
			message='\tGame installation path setting\n\n'
			message="$message"'\tThis option accepts an absolute path only.\n\n'
		;;
	esac
	print_message 'info' '--%s %s\n' \
		'prefix' \
		'path'
	print_message 'info' "$message"
}

# display --package option usage
# USAGE: help_package
help_package() {
	## TODO: "egentoo" format should be included.
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tChoix du type de paquet à construire\n\n'
			message="$message"'\t%s\tpaquet .pkg.tar (Arch Linux)\n'
			message="$message"'\t%s\tpaquet .deb (Debian, Ubuntu)\n'
			message="$message"'\t%s\tpaquet .tbz2 (Gentoo)\n\n'
		;;
		('en'|*)
			message='\tGenerated package type selection\n\n'
			message="$message"'\t%s\t.pkg.tar package (Arch Linux)\n'
			message="$message"'\t%s\t.deb package (Debian, Ubuntu)\n'
			message="$message"'\t%s\t.tbz2 package (Gentoo)\n\n'
		;;
	esac
	print_message 'info' '--%s %s\n' \
		'prefix' \
		'arch|deb|gentoo'
	print_message 'info' "$message" \
		'arch' \
		'deb' \
		'gentoo'
}

# display --no-icons option usage
# USAGE: help_icons
help_icons() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tNe pas inclure les icônes du jeu.\n\n'
		;;
		('en'|*)
			message='\tDo not include game icons.\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'no-icons'
	print_message 'info' "$message"
}

# display --overwrite option usage
# USAGE: help_overwrite
help_overwrite() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tRemplace les paquets si ils existent déjà.\n\n'
		;;
		('en'|*)
			message='\tReplace packages if they already exist.\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'overwrite'
	print_message 'info' "$message"
}

# display --output-dir option usage
# USAGE: help_output_dir
help_output_dir() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tDéfinit le répertoire de destination des paquets générés.\n\n'
		;;
		('en'|*)
			message='\tSet the output directory for generated packages.\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'output-dir'
	print_message 'info' "$message"
}

# Display the --collection-path option usage
# USAGE: help_collection_path
help_collection_path() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tLimiter la recherche de scripts de prise en charge de jeux au chemin donné.\n\n'
			;;
		('en'|*)
			message='\tLimit the search for game scripts to a given path.\n\n'
			;;
	esac
	print_message 'info' '--%s\n' \
		'collection-path'
	print_message 'info' "$message"
}

# display --show-game-script option usage
# USAGE: help_show_game_script
help_show_game_script() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tAffiche uniquement le chemin vers le script à utiliser, sans le lancer.\n\n'
			;;
		('en'|*)
			message='\tOnly displays the name of the script to use, without running it.\n\n'
			;;
	esac
	print_message 'info' '--%s\n' \
		'show-game-script'
	print_message 'info' "$message"
}

# display --no-mtree option usage
# USAGE: help_no_mtree
help_no_mtree() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tNe crée pas de fichier .MTREE pour les paquets Arch Linux.\n\n'
			;;
		('en'|*)
			message='\tDo not make .MTREE file in Arch Linux packages\n\n'
			;;
	esac
	print_message 'info' '--%s\n' \
		'no-mtree'
	print_message 'info' "$message"
}

# Display --tmpdir option usage
# USAGE: help_tmpdir
help_tmpdir() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tDéfinit le répertoire utilisé pour le stockage des fichiers temporaire.\n'
			message="$message"'\tLa valeur par défaut est : %s\n\n'
		;;
		('en'|*)
			message='\tSet the directory used for temporary files storage.\n'
			message="$message"'\tDefault value is: %s\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'tmpdir'
	print_message 'info' "$message" \
		"${TMPDIR:-/tmp}"
}

# Display --no-free-space-check option usage
# USAGE: help_skipfreespacecheck
help_skipfreespacecheck() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tNe pas tester lʼespace libre disponible.\n\n'
		;;
		('en'|*)
			message='\tDo not check for free space.\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'no-free-space-check'
	print_message 'info' "$message"
}

# Display --config-file option usage
# USAGE: help_configfile
help_configfile() {
	local config_file_path
	config_file_path=$(configuration_file_default_path)

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tDéfinit le fichier de configuration à utiliser.\n'
			message="$message"'\tLe fichier par défaut est : %s\n\n'
			;;
		('en'|*)
			message='\tSet the configuration file to use.\n'
			message="$message"'\tDefault file is: %s\n\n'
			;;
	esac
	print_message 'info' '--%s\n' \
		'config-file'
	print_message 'info' "$message" \
		"$config_file_path"
}

# Display --list-packages option usage
# USAGE: help_listpackages
help_listpackages() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tAffiche la liste des paquets à construire.\n\n'
			;;
		('en'|*)
			message='\tPrint the list of packages to build.\n\n'
			;;
	esac
	print_message 'info' '--%s\n' \
		'list-packages'
	print_message 'info' "$message"
}

# Display --list-requirements option usage
# USAGE: help_listrequirements
help_listrequirements() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tAffiche la liste des commandes nécessaire à la construction de paquets à partir de lʼarchive donnée.\n\n'
		;;
		('en'|*)
			message='\tPrint the list of commands required to build packages from the given archive.\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'list-requirements'
	print_message 'info' "$message"
}

# Display --list-available-scripts option usage
# USAGE: help_listavailablescripts
help_listavailablescripts() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tAffiche la liste des scripts de prise en charge de jeux disponibles sur ce système.\n\n'
		;;
		('en'|*)
			message='\tPrint the list of game scripts available on this system.\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'list-available-scripts'
	print_message 'info' "$message"
}

# Display --list-supported-games option usage
# USAGE: help_listsupportedgames
help_listsupportedgames() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\tAffiche la liste des jeux pris en charge.\n'
			message="$message"'\tAttention : cette opération peut prendre plusieurs minutes.\n\n'
		;;
		('en'|*)
			message='\tPrint the list of supported games.\n'
			message="$message"'\tWarning: this operation can take several minutes.\n\n'
		;;
	esac
	print_message 'info' '--%s\n' \
		'list-supported-games'
	print_message 'info' "$message"
}

