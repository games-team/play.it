# Error - $script_version does not follow the expected format
# USAGE: error_invalid_version_string $version_string
error_invalid_version_string() {
	local version_string
	version_string="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La valeur suivante de $script_version ne respecte pas le format attendu : %s\n'
			message="$message"'Le format correct est "YYYYMMDD.N", avec "YYYYMMDD" la date de dernière édition du script, et "N" un nombre incrémenté si le script est édité plusieurs fois dans la même journée.\n'
		;;
		('en'|*)
			message='The following $script_version value does not follow the expected format: %s\n'
			message="$message"'The correct format is "YYYYMMDD.N", with "YYYYMMDD" the date of the last edition of the script, and "N" a number incremented if the script is edited multiple times at the same date.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$version_string"
}

# Error - No source of game scripts could be found
# USAGE: error_no_collection
error_no_collection() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Aucune collection ajoutant la prise en charge de jeux n’a été trouvée.\n'
			message="$message"'Des instructions pour en ajouter peuvent se trouver sur la page suivante :\n'
			message="$message"'%s\n'
		;;
		('en'|*)
			message='No collection adding support for games has been found.\n'
			message="$message"'Installation instructions for games collections can be found on the following page:\n'
			message="$message"'%s\n'
		;;
	esac
	print_message 'error' "$message" \
		'https://git.dotslashplay.it/scripts/about/#game-scripts'
}

