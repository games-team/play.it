# Arch Linux - Print the package names providing the commands required by the given package
# USAGE: archlinux_dependencies_all_commands $package
# RETURN: a list of Arch Linux package names,
#         one per line
archlinux_dependencies_all_commands() {
	local package
	package="$1"

	local required_commands
	required_commands=$(dependencies_list_commands "$package")
	# Return early if the current package does not require any command
	if [ -z "$required_commands" ]; then
		return 0
	fi

	local command packages_list required_packages
	packages_list=''
	while read -r command; do
		required_packages=$(archlinux_dependencies_single_command "$command")
		packages_list="$packages_list
		$required_packages"
	done <<- EOL
	$(printf '%s' "$required_commands")
	EOL

	printf '%s' "$packages_list" | list_clean
}

# Arch Linux - Print the package names providing the required command
# USAGE: archlinux_dependencies_single_command $required_command
# RETURN: a list of Arch Linux package names,
#         one per line
archlinux_dependencies_single_command() {
	local required_command
	required_command="$1"

	local package_names
	case "$required_command" in
		('7za')
			package_names='
			extra/p7zip'
		;;
		('corsix-th')
			package_names='
			corsix-th'
		;;
		('dos2unix')
			package_names='
			dos2unix'
		;;
		('dosbox')
			package_names='
			dosbox'
		;;
		('godot3-runner')
			# The Godot 3 runtime is not provided by Arch Linux, not even in the AUR
			return 0
		;;
		('java')
			package_names='
			jre8-openjdk'
		;;
		('mono')
			package_names='
			mono'
		;;
		('mpv')
			package_names='
			mpv'
		;;
		('openmw-iniimporter')
			package_names='
			openmw'
		;;
		('openmw-launcher')
			package_names='
			openmw'
		;;
		('pulseaudio')
			package_names='
			pulseaudio'
		;;
		('renpy')
			package_names='
			renpy'
		;;
		('scummvm')
			package_names='
			scummvm'
		;;
		('sed')
			package_names='
			sed'
		;;
		('setxkbmap')
			package_names='
			xorg-setxkbmap'
		;;
		('terminal_wrapper')
			package_names='
			xterm'
		;;
		('vcmilauncher')
			package_names='
			vcmi'
		;;
		('wine')
			package_names='
			wine'
		;;
		('winetricks')
			package_names='
			winetricks'
		;;
		('xgamma')
			package_names='
			xorg-xgamma'
		;;
		('xrandr')
			package_names='
			xorg-xrandr'
		;;
		(*)
			dependencies_unknown_command_add "$required_command"
			return 0
		;;
	esac

	printf '%s' "$package_names"
}

