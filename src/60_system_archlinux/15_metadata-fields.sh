# Arch Linux - Print the value of the "pkgname" field
# USAGE: archlinux_field_pkgname $package
# RETURN: the field value
archlinux_field_pkgname() {
	local package
	package="$1"

	package_id "$package"
}

# Arch Linux - Print the value of the "pkgver" field
# USAGE: archlinux_field_pkgver $package
# RETURN: the field value
archlinux_field_pkgver() {
	## The package value is not actually used by this function,
	## but it is still passed for consistency with other archlinux_field_* functions.
	local package
	package="$1"

	package_version
}

# Arch Linux - Print the value of the "packager" field
# USAGE: archlinux_field_packager $package
# RETURN: the field value
archlinux_field_packager() {
	## The package value is not actually used by this function,
	## but it is still passed for consistency with other archlinux_field_* functions.
	local package
	package="$1"

	package_maintainer
}

# Arch Linux - Print the value of the "builddate" field
# USAGE: archlinux_field_builddate $package
# RETURN: the field value
archlinux_field_builddate() {
	## The package value is not actually used by this function,
	## but it is still passed for consistency with other archlinux_field_* functions.
	local package
	package="$1"

	date '+%s'
}

# Arch Linux - Print the value of the "size" field
# USAGE: archlinux_field_size $package
# RETURN: the field value
archlinux_field_size() {
	local package
	package="$1"

	local package_path
	package_path=$(package_path "$package")
	du --total --block-size=1 --summarize "$package_path" |
		tail --lines=1 |
		cut --fields=1
}

# Arch Linux - Print the value of the "arch" field
# USAGE: archlinux_field_arch $package
# RETURN: the field value
archlinux_field_arch() {
	local package
	package="$1"

	local package_architecture package_architecture_string
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32'|'64')
			package_architecture_string='x86_64'
		;;
		('all')
			package_architecture_string='any'
		;;
	esac

	printf '%s' "$package_architecture_string"
}

# Arch Linux - Print the value of the "pkgdesc" field
# USAGE: archlinux_field_pkgdesc $package
# RETURN: the field value
archlinux_field_pkgdesc() {
	local package
	package="$1"

	local game_name package_description script_version_string
	game_name=$(game_name)
	package_description=$(package_description "$package")
	script_version_string=$(script_version)

	printf '%s' "$game_name"
	if [ -n "$package_description" ]; then
		printf -- ' - %s' "$package_description"
	fi
	printf -- ' - ./play.it script version %s' "$script_version_string"
}

# Arch Linux - Print the value of the "depend" fields
# Each package name is displayed on its own line.
# USAGE: archlinux_field_depend $package
# RETURN: the field value
archlinux_field_depend() {
	local package
	package="$1"

	dependencies_archlinux_full_list "$package"
}

# Arch Linux - Print the value of the "provides" fields
# Each package name is displayed on its own line.
# These values are used for the "conflict" fields too.
# USAGE: archlinux_field_provides $package
# RETURN: the field value
archlinux_field_provides() {
	local package
	package="$1"

	local package_provides
	package_provides=$(package_provides "$package")

	local package_architecture
	package_architecture=$(package_architecture "$package")
	if [ "$package_architecture" = '32' ]; then
		local package_id package_name_32
		package_id=$(package_id "$package")
		package_name_32=$(printf '%s' "$package_id" | sed 's/^lib32-//')
		package_provides="$package_provides
		$package_name_32"
	fi

	# Return early if there is no package name provided
	if [ -z "$package_provides" ]; then
		return 0
	fi

	printf '%s\n' $package_provides
}

