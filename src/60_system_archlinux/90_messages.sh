# print mtree computation message
# USAGE: info_package_mtree_computation $package
info_package_mtree_computation() {
	local package
	package="$1"

	local package_path
	package_path=$(package_path "$package")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Création du fichier .MTREE pour %s…\n'
		;;
		('en'|*)
			message='Creating .MTREE file for %s…\n'
		;;
	esac
	print_message 'info' "$message" \
		"$package_path"
}
