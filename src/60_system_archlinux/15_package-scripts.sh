# Arch Linux - Print the contents of the .INSTALL script
# USAGE: archlinux_script_install $package
# RETURN: the contents of the .INSTALL file,
#         spanning over several lines
archlinux_script_install() {
	local package
	package="$1"

	# Print the definitions of post_install and post_upgrade.
	local postinst_actions postinst_warnings
	postinst_actions=$(package_postinst_actions "$package")
	postinst_warnings=$(package_postinst_warnings "$package")
	if [ -n "$postinst_actions" ] || [ -n "$postinst_warnings" ]; then
		cat <<- EOF
		post_install() {
		EOF
		## Include actions that should be run.
		if [ -n "$postinst_actions" ]; then
			printf '%s\n' "$postinst_actions"
		fi
		## Include warnings that should be displayed.
		if [ -n "$postinst_warnings" ]; then
			local warning_line
			while read -r warning_line; do
				printf 'printf "Warning: %%s\\n" "%s"\n' "$warning_line"
			done <<- EOL
			$(printf '%s' "$postinst_warnings")
			EOL
		fi
		cat <<- EOF
		}

		post_upgrade() {
		post_install
		}
		EOF
	fi

	# Print the definitions of pre_remove and pre_upgrade.
	local prerm_actions
	prerm_actions=$(package_prerm_actions "$package")
	if [ -n "$prerm_actions" ]; then
		cat <<- EOF
		pre_remove() {
		$prerm_actions
		}

		pre_upgrade() {
		pre_remove
		}
		EOF
	fi
}

