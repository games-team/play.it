# Visionaire - Print the Visionaire name for the current game
# This function should not fail if no Visionaire name is set for the current game,
# so it can be used to automatically detect games using the "visionaire" type variant.
# USAGE: visionaire_name
# RETURN: the Visionaire name, a string that can include spaces,
#         or an empty string if none is set
visionaire_name() {
	context_value 'VISIONAIRE_NAME'
}

# Visionaire - Print the default path to include files from
# USAGE: visionaire_content_path $content_id
# RETURN: a list of paths relative to the path for the given identifier,
#         line breaks are used as separator between each item,
#         this list can include globbing patterns,
#         this list can be empty
visionaire_content_path() {
	local content_id
	content_id="$1"

	local applications_list application application_type
	applications_list=$(applications_list)
	if [ -z "$applications_list" ]; then
		error_applications_list_empty
	fi
	application=$(printf '%s' "$applications_list" | head --lines=1)
	application_type=$(application_type "$application")

	local content_path content_path_default
	content_path_default=$(content_path_default)
	case "$content_id" in
		('LIBS_BIN')
			case "$application_type" in
				('native')
					content_path="${content_path_default}/libs64"
				;;
			esac
		;;
		('DOC_DATA')
			case "$application_type" in
				('native')
					content_path="${content_path_default}/documents"
				;;
				('wine')
					content_path="${content_path_default}/documents"
				;;
			esac
		;;
	esac

	printf '%s' "${content_path:-}"
}

# Visionaire - Print the list of files to include from the archive for a given identifier
# USAGE: visionaire_content_files $content_id
# RETURN: a list of paths relative to the path for the given identifier,
#         line breaks are used as separator between each item,
#         this list can include globbing patterns,
#         this list can be empty
visionaire_content_files() {
	local content_id
	content_id="$1"

	local visionaire_name
	visionaire_name=$(visionaire_name)

	local applications_list application application_type
	applications_list=$(applications_list)
	if [ -z "$applications_list" ]; then
		error_applications_list_empty
	fi
	application=$(printf '%s' "$applications_list" | head --lines=1)
	application_type=$(application_type "$application")

	local content_files
	case "$content_id" in
		('LIBS_BIN')
			case "$application_type" in
				('native')
					content_files='
					libavcodec.so.56
					libavdevice.so.56
					libavfilter.so.5
					libavformat.so.56
					libavutil.so.54
					libswresample.so.1
					libswscale.so.3'
				;;
			esac
		;;
		('GAME_BIN')
			case "$application_type" in
				('native')
					content_files="
					config.ini
					$visionaire_name"
				;;
				('wine')
					content_files="
					config.ini
					avcodec-*dll
					avformat-*.dll
					avutil-*.dll
					libsndfile-*.dll
					openal.dll
					openal32.dll
					sdl.dll
					sdl2.dll
					swresample-*.dll
					swscale-*.dll
					zlib1.dll
					${visionaire_name}.exe"
				;;
			esac
		;;
		('GAME_DATA')
			case "$application_type" in
				('native')
					content_files='
					characters
					lua
					scenes
					videos
					data.vis'
				;;
				('wine')
					content_files='
					characters
					lua
					scenes
					videos
					data.vis
					banner.jpg
					folder.jpg
					languages.xml'
				;;
			esac
		;;
		('DOC_DATA')
			case "$application_type" in
				('native')
					content_files='
					licenses'
				;;
				('wine')
					content_files='
					licenses'
				;;
			esac
		;;
	esac

	printf '%s' "${content_files:-}"
}

