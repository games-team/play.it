# Visionaire - Prevent the use of wayland SDL video driver, for use with WINE games
# USAGE: visionaire_tweak_sdl_wine
visionaire_tweak_sdl_wine() {
	cat <<- 'EOF'
	# Prevent the use of wayland SDL video driver
	if [ "${SDL_VIDEODRIVER:-}" = 'wayland' ]; then
	    unset SDL_VIDEODRIVER
	fi

	EOF
}

