# Visionaire - Get the list of applications
# USAGE: visionaire_applications_list
# RETURN: a list of application identifiers,
#         separated by line breaks
visionaire_applications_list() {
	printf '%s\n' \
		'APP_MAIN'
}

# Visionaire - Get the name of the game binary
# USAGE: visionaire_application_exe
# RETURN: the game binary file name,
#         or an empty string
visionaire_application_exe() {
	## WARNING: We can not rely on the application type here,
	##          to avoid a loop between application_exe and application_type.
	local application_exe visionaire_name filename_candidates_list filename_candidate filename_path
	visionaire_name=$(visionaire_name)
	filename_candidates_list="
	${visionaire_name}
	${visionaire_name}.exe"
	while read -r filename_candidate; do
		## Skip empty lines.
		if [ -z "$filename_candidate" ]; then
			continue
		fi
		## Do not throw an error if no path is found for the current candidate.
		filename_path=$(application_exe_path "$filename_candidate") 2>/dev/null || true
		if [ -n "$filename_path" ]; then
			application_exe="$filename_candidate"
			break
		fi
	done <<- EOL
	$(printf '%s' "$filename_candidates_list")
	EOL

	printf '%s' "${application_exe:-}"
}

