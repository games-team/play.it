# Visionaire - Get the default list of packages to build
# USAGE: visionaire_packages_list
# RETURN: a list of packages identifiers,
#         separated by line breaks
visionaire_packages_list() {
	printf '%s\n' \
		'PKG_BIN' \
		'PKG_DATA'
}

# Visionaire - Get the default package id for the given package
# USAGE: visionaire_package_id $package
# RETURN: a package id,
#         or an empty string if there is not default value for the given package
visionaire_package_id() {
	local package
	package="$1"

	local package_id game_id
	game_id=$(game_id)
	case "$package" in
		('PKG_DATA')
			package_id="${game_id}-data"
		;;
	esac

	printf '%s' "${package_id:-}"
}

# Visionaire - Get the default package description for the given package
# USAGE: visionaire_package_description $package
# RETURN: a package description,
#         or an empty string if there is not default value for the given package
visionaire_package_description() {
	local package
	package="$1"

	local package_description
	case "$package" in
		('PKG_DATA')
			package_description='data'
		;;
	esac

	printf '%s' "${package_description:-}"
}

# Visionaire - Get the list of dependencies on siblings for the given package
# USAGE: visionaire_package_dependencies_siblings $package
# RETURN: a list of package identifiers,
#         separated by line breaks
visionaire_package_dependencies_siblings() {
	local package
	package="$1"

	local package_dependencies
	case "$package" in
		('PKG_BIN')
			package_dependencies='
			PKG_DATA'
		;;
	esac

	printf '%s' "${package_dependencies:-}" | list_clean
}

# Visionaire - Get the list of native libraries dependencies for the given package
# USAGE: visionaire_package_dependencies_native_libraries $package
# RETURN: a list of native libraries,
#         separated by line breaks
visionaire_package_dependencies_native_libraries() {
	local package
	package="$1"

	local package_dependencies
	case "$package" in
		('PKG_BIN')
			package_dependencies='
			libc.so.6
			libdl.so.2
			libgcc_s.so.1
			libGL.so.1
			libm.so.6
			libopenal.so.1
			libpthread.so.0
			librt.so.1
			libSDL2-2.0.so.0
			libstdc++.so.6'
		;;
	esac

	printf '%s' "${package_dependencies:-}" | list_clean
}

