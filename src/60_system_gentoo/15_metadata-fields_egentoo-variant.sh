# Gentoo ("egentoo" variant) - Print the content of the "KEYWORDS" field
# USAGE: egentoo_field_keywords $package…
# RETURN: the field value
egentoo_field_keywords() {
	local keywords package package_architecture
	keywords='x86 amd64'
	for package in "$@"; do
		package_architecture=$(package_architecture "$package")
		case "$package_architecture" in
			('32')
				printf '%s' '-* x86 amd64'
				return 0
			;;
			('64')
				keywords='-* amd64'
			;;
		esac
	done

	printf '%s' "$keywords"
}

# Gentoo ("egentoo" variant) - Print the content of the "SRC_URI" field
# USAGE: egentoo_field_srcuri $package…
# RETURN: the field value
egentoo_field_srcuri() {
	## The list of packages is ignored by this function,
	## it is only passed for consistency with other egentoo_field_* functions.

	package_filename="$(egentoo_package_name).tar"

	local option_compression
	option_compression=$(option_value 'compression')
	case $option_compression in
		('speed')
			package_filename="${package_filename}.gz"
		;;
		('size')
			package_filename="${package_filename}.bz2"
		;;
	esac

	printf '%s' "$package_filename"
}

# Gentoo ("egentoo" variant) - Print the content of the "RDEPEND" field
# USAGE: egentoo_field_rdepend $package…
# RETURN: the field value
egentoo_field_rdepend() {
	local package package_architecture package_64bits package_32bits package_data
	package_64bits=''
	package_32bits=''
	package_data=''
	for package in "$@"; do
		package_architecture=$(package_architecture "$package")
		case "$package_architecture" in
			('32')
				package_32bits="$package"
			;;
			('64')
				package_64bits="$package"
			;;
			(*)
				package_data="$package"
			;;
		esac
	done

	if [ -n "$package_64bits" ]; then
		gentoo_field_rdepend "$package_64bits"
	elif [ -n "$package_32bits" ]; then
		gentoo_field_rdepend "$package_32bits"
	elif [ -n "$package_data" ]; then
		gentoo_field_rdepend "$package_data"
	fi
}

