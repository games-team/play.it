# Ren'Py - Print the launcher script content
# USAGE: renpy_launcher $application
renpy_launcher() {
	local application
	application="$1"

	local prefix_type
	prefix_type=$(application_prefix_type "$application")
	case "$prefix_type" in
		('symlinks')
			launcher_headers

			# Set the paths that should be available to the generated launcher
			launcher_init_paths "$application"

			# Generate the game prefix
			prefix_generate_links_farm
			launcher_prefix_symlinks_build

			# Set up the paths diversion to persistent storage
			persistent_storage_initialization
			persistent_storage_common
			persistent_path_diversion
			persistent_storage_update_directories
			persistent_storage_update_files

			renpy_launcher_run "$application"

			launcher_exit
		;;
		('none')
			launcher_headers

			# Set the paths that should be available to the generated launcher
			launcher_init_paths "$application"

			renpy_launcher_run "$application"
			launcher_exit
		;;
		(*)
			error_launchers_prefix_type_unsupported "$application"
			return 1
		;;
	esac
}

# Ren'Py - Print the commands running the game, either from the read-only system path or from a symlinks farm prefix
# USAGE: renpy_launcher_run $application
renpy_launcher_run() {
	local application
	application="$1"

	local prefix_type execution_path
	prefix_type=$(application_prefix_type "$application")
	case "$prefix_type" in
		('symlinks')
			execution_path='$PATH_PREFIX'
		;;
		('none')
			execution_path='$PATH_GAME_DATA'
		;;
	esac
	cat <<- EOF
	# Run the game

	cd "$execution_path"

	EOF

	application_prerun "$application"
	game_exec_line "$application"
	application_postrun "$application"
}

# Ren'Py - Print the line starting the game
# USAGE: renpy_exec_line $application
# RETURN: the command to execute
renpy_exec_line() {
	## The application identifier is not actually used for Ren'Py games,
	## it is only passed for consistency with other *_exec_line functions.
	local application
	application="$1"

	cat <<- 'EOF'
	renpy .
	EOF
}

