# print common part of packages installation instructions
# USAGE: information_installation_instructions_common $game_name
information_installation_instructions_common() {
	local game_name
	game_name="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\nInstallez "%s" en lançant la série de commandes suivantes en root :\n'
		;;
		('en'|*)
			message='\nInstall "%s" by running the following commands as root:\n'
		;;
	esac
	print_message 'info' "$message" \
		"$game_name"
}

# print variant precision for packages installation instructions
# USAGE: information_installation_instructions_variant $variant
information_installation_instructions_variant() {
	local variant
	variant="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='\nversion %s :\n'
		;;
		('en'|*)
			message='\n%s version:\n'
		;;
	esac
	print_message 'info' "$message" \
		"$variant"
}

# Display a list of unknown runtime commands from packages dependencies
# USAGE: warning_dependencies_unknown_commands
warning_dependencies_unknown_commands() {
	local messages_language message1 message2
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message1='Certaines dépendances de ce jeu ne sont pas encore prises en charge par ./play.it'
			message1="$message1"', voici la liste de celles qui ont été ignorées :\n'
			message2='Merci de signaler cette liste sur notre système de suivi :\n%s\n'
		;;
		('en'|*)
			message1='Some dependencies of this game are not supported by ./play.it yet'
			message1="$message1"', here are the ones that have been skipped:\n'
			message2='Please report this list on our issues tracker:\n%s\n'
		;;
	esac
	print_message 'warning' "$message1"
	local unknown_command
	while read -r unknown_command; do
		print_message 'info' '- %s\n' \
			"$unknown_command"
	done <<- EOL
	$(dependencies_unknown_commands_list)
	EOL
	print_message 'info' "$message2" \
		"$PLAYIT_BUG_TRACKER_URL"
}

# Display a list of unknown libraries from packages dependencies
# USAGE: warning_dependencies_unknown_libraries
warning_dependencies_unknown_libraries() {
	local messages_language message1 message2
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message1='Certaines dépendances de ce jeu ne sont pas encore prises en charge par ./play.it'
			message1="$message1"', voici la liste de celles qui ont été ignorées :\n'
			message2='Merci de signaler cette liste sur notre système de suivi :\n%s\n'
		;;
		('en'|*)
			message1='Some dependencies of this game are not supported by ./play.it yet'
			message1="$message1"', here are the ones that have been skipped:\n'
			message2='Please report this list on our issues tracker:\n%s\n'
		;;
	esac
	print_message 'warning' "$message1"
	local unkown_library
	while read -r unkown_library; do
		print_message 'info' '- %s\n' \
			"$unkown_library"
	done <<- EOL
	$(dependencies_unknown_libraries_list)
	EOL
	print_message 'info' "$message2" \
		"$PLAYIT_BUG_TRACKER_URL"
}

# Display a list of unknown Mono libraries from packages dependencies
# USAGE: warning_dependencies_unknown_mono_libraries
warning_dependencies_unknown_mono_libraries() {
	local messages_language message1 message2
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message1='Certaines dépendances Mono de ce jeu ne sont pas encore prises en charge par ./play.it'
			message1="$message1"', voici la liste de celles qui ont été ignorées :\n'
			message2='Merci de signaler cette liste sur notre système de suivi :\n%s\n'
		;;
		('en'|*)
			message1='Some Mono dependencies of this game are not supported by ./play.it yet'
			message1="$message1"', here are the ones that have been skipped:\n'
			message2='Please report this list on our issues tracker:\n%s\n'
		;;
	esac
	print_message 'warning' "$message1"
	local unkown_mono_library
	while read -r unkown_mono_library; do
		print_message 'info' '- %s\n' \
			"$unkown_mono_library"
	done <<- EOL
	$(dependencies_unknown_mono_libraries_list)
	EOL
	print_message 'info' "$message2" \
		"$PLAYIT_BUG_TRACKER_URL"
}

# Display a list of unknown GStreamer media formats from packages dependencies
# USAGE: warning_dependencies_unknown_gstreamer_media_formats
warning_dependencies_unknown_gstreamer_media_formats() {
	local messages_language message1 message2
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message1='Certains formats multimédia requis par ce jeu ne sont pas encore pris en charge par ./play.it'
			message1="$message1"', voici la liste de ceux qui ont été ignorés :\n'
			message2='Merci de signaler cette liste sur notre système de suivi :\n%s\n'
		;;
		('en'|*)
			message1='Some media formats required by this game are not supported by ./play.it yet'
			message1="$message1"', here are the ones that have been skipped:\n'
			message2='Please report this list on our issues tracker:\n%s\n'
		;;
	esac
	print_message 'warning' "$message1"
	local media_format
	while read -r media_format; do
		print_message 'info' '- %s\n' \
			"$media_format"
	done <<- EOL
	$(dependencies_unknown_gstreamer_media_formats_list)
	EOL
	print_message 'info' "$message2" \
		"$PLAYIT_BUG_TRACKER_URL"
}

