# Debian - Print the content of the "Package" field
# USAGE: debian_field_package $package
# RETURN: the field content
debian_field_package() {
	local package
	package="$1"

	package_id "$package"
}

# Debian - Print the content of the "Source" field
# USAGE: debian_field_source $package
# RETURN: the field content
debian_field_source() {
	## The package argument is not actually used,
	## but is passed for consistency with other debian_field_* functions.
	local package
	package="$1"

	## Including the version should make upgrades easier when using `apt install --with-source (…)`.
	local game_id package_version
	game_id=$(game_id)
	package_version=$(package_version)

	printf '%s (%s)' "$game_id" "$package_version"
}

# Debian - Print the content of the "Version" field
# USAGE: debian_field_version $package
# RETURN: the field content
debian_field_version() {
	## The package argument is not actually used,
	## but is passed for consistency with other debian_field_* functions.
	local package
	package="$1"

	package_version
}

# Debian - Print the content of the "Architecture" field
# USAGE: debian_field_architecture $package
# RETURN: the field content
debian_field_architecture() {
	local package
	package="$1"

	local package_architecture package_architecture_string
	package_architecture=$(package_architecture "$package")
	case "$package_architecture" in
		('32')
			package_architecture_string='i386'
		;;
		('64')
			package_architecture_string='amd64'
		;;
		('all')
			package_architecture_string='all'
		;;
	esac

	printf '%s' "$package_architecture_string"
}

# Debian - Print the content of the "Maintainer" field
# USAGE: debian_field_maintainer $package
# RETURN: the field content
debian_field_maintainer() {
	## The package argument is not actually used,
	## but is passed for consistency with other debian_field_* functions.
	local package
	package="$1"

	package_maintainer
}

# Debian - Print the content of the "Installed-Size" field
# USAGE: debian_field_installedsize $package
# RETURN: the field content
debian_field_installedsize() {
	local package
	package="$1"

	# Compute the package size, in kilobytes
	local package_path
	package_path=$(package_path "$package")
	du --total --block-size=1K --summarize "$package_path" |
		tail --lines=1 |
		cut --fields=1
}

# Debian - Print the content of the "Provides" field
# This value is used for the fields "Conflicts" and "Replaces" too.
# USAGE: debian_field_provides $package
# RETURN: the field content
debian_field_provides() {
	local package
	package="$1"

	local package_provides
	package_provides=$(package_provides "$package")

	# Return early if there is no package name provided
	if [ -z "$package_provides" ]; then
		return 0
	fi

	local provides_list package_name
	while read -r package_name; do
		if [ -z "${provides_list:-}" ]; then
			provides_list="$package_name"
		else
			provides_list="$provides_list, $package_name"
		fi
	done <<- EOL
	$(printf '%s' "$package_provides")
	EOL

	printf '%s' "$provides_list"
}

# Debian - Print the content of the "Depends" field
# USAGE: debian_field_depends $package
# RETURN: the field content
debian_field_depends() {
	local package
	package="$1"

	local dependencies_list first_item_displayed dependency_string
	dependencies_list=$(dependencies_debian_full_list "$package")
	first_item_displayed=0
	while read -r dependency_string; do
		if [ -z "$dependency_string" ]; then
			continue
		fi
		if [ "$first_item_displayed" -eq 0 ]; then
			printf '%s' "$dependency_string"
			first_item_displayed=1
		else
			printf ', %s' "$dependency_string"
		fi
	done <<- EOF
	$(printf '%s' "$dependencies_list")
	EOF
}

# Debian - Print the content of the "Description" field
# USAGE: debian_field_description $package
# RETURN: the field content,
#         spanning over multiple lines
debian_field_description() {
	local package
	package="$1"

	local game_name package_description script_version_string
	game_name=$(game_name)
	package_description=$(package_description "$package")
	script_version_string=$(script_version)

	printf '%s' "$game_name"
	if [ -n "$package_description" ]; then
		printf -- ' - %s' "$package_description"
	fi
	printf '\n ./play.it script version %s' "$script_version_string"
}

