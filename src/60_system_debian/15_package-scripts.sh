# Debian - Print the content of the DEBIAN/postinst script for the given package
# USAGE: debian_script_postinst $package
# RETURN: the contents of the DEBIAN/postinst file,
#         spanning over multiple lines
debian_script_postinst() {
	local package
	package="$1"

	cat <<- EOF
	#!/bin/sh
	set -o errexit

	EOF
	## Include actions that should be run.
	package_postinst_actions "$package"
	## Include warnings that should be displayed.
	local warning_messages
	warning_messages=$(package_postinst_warnings "$package")
	if [ -n "$warning_messages" ]; then
		local warning_line
		while read -r warning_line; do
			printf 'printf "Warning: %%s\\n" "%s"\n' "$warning_line"
		done <<- EOL
		$(printf '%s' "$warning_messages")
		EOL
	fi
	cat <<- EOF

	exit 0
	EOF
}

# Debian - Print the content of the DEBIAN/prerm script for the given package
# USAGE: debian_script_prerm $package
# RETURN: the contents of the DEBIAN/prerm file,
#         spanning over multiple lines
debian_script_prerm() {
	local package
	package="$1"

	cat <<- EOF
	#!/bin/sh
	set -o errexit

	EOF
	## Include actions that should be run.
	package_prerm_actions "$package"
	cat <<- EOF

	exit 0
	EOF
}

