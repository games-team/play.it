# Force the use of the system SDL library, for use with native games
# USAGE: launcher_tweak_sdl_override
launcher_tweak_sdl_override() {
	local path_libraries
	path_libraries=$(path_libraries_system)
	cat <<- EOF
	# Force the use of the system SDL library
	if [ -z "\${SDL_DYNAMIC_API:-}" ]; then
	    export SDL_DYNAMIC_API="${path_libraries}/libSDL2-2.0.so.0"
	fi

	EOF
}

