# Populate prefix with symbolic links to all game files
# USAGE: prefix_generate_links_farm
prefix_generate_links_farm() {
	cat <<- 'EOF'
	# Populate prefix with symbolic links to all game files
	prefix_generate_links_farm() {
	    ## Remove links to game directories
	    (
	        cd "$PATH_GAME_DATA"
	        find . -type d | while read -r directory; do
	            if [ -h "${PATH_PREFIX}/${directory}" ]; then
	                rm "${PATH_PREFIX}/${directory}"
	            fi
	        done
	        unset directory
	    )

	    ## Populate prefix with links to all game files.
	    cp \
	        --dereference --no-target-directory --recursive --remove-destination --symbolic-link \
	        "$PATH_GAME_DATA" "$PATH_PREFIX"

	    ## Remove dangling links and non-game empty directories.
	    (
	        cd "$PATH_PREFIX"
	        find . -type l | while read -r link; do
	            if [ ! -e "$link" ]; then
	                rm "$link"
	            fi
	        done
	        find . -depth -type d | while read -r directory; do
	            if [ ! -e "${PATH_GAME_DATA}/${directory}" ]; then
	                rmdir --ignore-fail-on-non-empty "$directory"
	            fi
	        done
	        unset link directory
	    )
	}

	EOF
}

# Print the actions used to build a symlinks prefix
# USAGE: launcher_prefix_symlinks_build
launcher_prefix_symlinks_build() {
	cat  <<- 'EOF'
	# Build user prefix

	mkdir --parents "$PATH_PREFIX"
	prefix_generate_links_farm

	EOF
}

