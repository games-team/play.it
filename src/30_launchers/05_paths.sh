# Set the paths that should be available to the generated launcher
# USAGE: launcher_init_paths $application
launcher_init_paths() {
	# The application this launcher is generated for
	# It is used to get the application type and the prefix type
	local application
	application="$1"

	# Always include the path to the root of the read-only game data
	local path_game_data
	path_game_data=$(path_game_data)
	cat <<- EOF
	# Set the paths that sould be available throughout the launcher

	## Set the path to the root of the read-only game data
	PATH_GAME_DATA='${path_game_data}'

	EOF

	# Include the path to the persistent user data, for applications relying on it
	local application_prefix_type
	application_prefix_type=$(application_prefix_type "$application")
	case "$application_prefix_type" in
		('symlinks')
			launcher_path_persistent
		;;
	esac

	# Include the paths that are specific to the application type
	# This is set after the path to the persistent user data, so a path relative to $PATH_PERSISTENT could be used
	local application_type
	application_type=$(application_type "$application")
	case "$application_type" in
		('java')
			java_init_paths
		;;
		('mono')
			mono_init_paths
		;;
		('native')
			native_init_paths
		;;
		('wine')
			wine_init_paths
		;;
	esac

	# Include the path to the volatile game prefix, for applications relying on it
	# This is set after the type-specific paths, so a path relative to $PATH_WINEPREFIX could be used
	case "$application_prefix_type" in
		('symlinks')
			launcher_path_prefix
		;;
	esac

	# Include the path to the fake $HOME, for applications relying on it
	local fake_home_directories
	fake_home_directories=$(fake_home_persistent_directories)
	if [ -n "$fake_home_directories" ]; then
		launcher_path_fake_home
	fi
}

# Set the path to persistent user data
# USAGE: launcher_path_persistent
launcher_path_persistent() {
	local game_id
	game_id=$(game_id)
	cat <<- EOF
	## Set the path to persistent user data
	path_persistent() {
	    # The persistent user data path can be overriden using an environment variable
	    if [ -n "\${PLAYIT_PATH_PERSISTENT:-}" ]; then
	        printf '%s' "\$PLAYIT_PATH_PERSISTENT"
	        return 0
	    fi
	    # Compute the default path if none has been explicitly set
	    printf '%s/games/${game_id}' "\${XDG_DATA_HOME:=\$HOME/.local/share}"
	}
	PATH_PERSISTENT=\$(path_persistent)

	EOF
}

# Set the path to the volatile game prefix
# USAGE: launcher_path_prefix
launcher_path_prefix() {
	local game_id
	game_id=$(game_id)
	cat <<- EOF
	## Set the path to the volatile game prefix
	path_prefix() {
	    # The prefix path can be overriden using an environment variable
	    if [ -n "\${PLAYIT_PATH_PREFIX:-}" ]; then
	        printf '%s' "\$PLAYIT_PATH_PREFIX"
	        return 0
	    fi
	    # Compute the default prefix path if none has been explicitely set
	    printf '%s/play.it/prefixes/${game_id}' "\${XDG_CACHE_HOME:="\$HOME/.cache"}"
	}
	PATH_PREFIX=\$(path_prefix)

	EOF
}

# Set the path to the fake $HOME
# USAGE: launcher_path_fake_home
launcher_path_fake_home() {
	local game_id
	game_id=$(game_id)
	cat <<- EOF
	## Set the path to the fake \$HOME
	path_fake_home() {
	    # The fake \$HOME path can be overriden using an environment variable
	    if [ -n "\${PLAYIT_PATH_FAKE_HOME:-}" ]; then
	        printf '%s' "\$PLAYIT_PATH_FAKE_HOME"
	        return 0
	    fi
	    # Compute the default fake \$HOME path if none has been explicitely set
	    printf '%s/play.it/home/${game_id}' "\${XDG_CACHE_HOME:="\$HOME/.cache"}"
	}
	PATH_FAKE_HOME=\$(path_fake_home)

	EOF
}

# Set the paths to native libraries
# USAGE: launcher_paths_libraries
launcher_paths_libraries() {
	local path_system game_id
	path_system=$(path_libraries)
	game_id=$(game_id)

	cat <<- EOF
	## Set the paths to native libraries
	path_libraries_user() {
	    # The path to user libraries can be overriden using an environment variable
	    if [ -n "\${PLAYIT_PATH_LIBRARIES_USER:-}" ]; then
	        printf '%s' "\$PLAYIT_PATH_LIBRARIES_USER"
	        return 0
	    fi
	    # Compute the path to user libraries if none has been explicitely set
	    printf '%s/.local/lib/games/${game_id}' "\$HOME"
	}
	PATH_LIBRARIES_SYSTEM='$path_system'
	PATH_LIBRARIES_USER=\$(path_libraries_user)

	EOF
}

