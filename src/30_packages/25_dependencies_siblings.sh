# Print the list of sibling packages required by a given package
# USAGE: dependencies_list_siblings $package
# RETURNS: a list of package names,
#          one per line
dependencies_list_siblings() {
	local package
	package="$1"

	local dependencies_siblings
	dependencies_siblings=$(context_value "${package}_DEPENDENCIES_SIBLINGS")

	# Fall back on the default list of dependencies for the current game engine
	if [ -z "$dependencies_siblings" ]; then
		local game_engine
		game_engine=$(game_engine)
		case "$game_engine" in
			('visionaire')
				dependencies_siblings=$(visionaire_package_dependencies_siblings  "$package")
			;;
		esac
	fi

	# Return early if the current package does not require any sibling package
	if [ -z "$dependencies_siblings" ]; then
		return 0
	fi

	# Convert the package identifiers to full package names
	local sibling
	while read -r sibling; do
		package_id "$sibling"
		printf '\n'
	done <<- EOL
	$(printf '%s' "$dependencies_siblings" | list_clean)
	EOL
}

