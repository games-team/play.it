# Information: All packages are being built
# USAGE: information_packages_generation
information_packages_generation() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Géneration des paquets…\n'
		;;
		('en'|*)
			message='Packages generation…\n'
		;;
	esac
	print_message 'info' "$message"
}

# display a notification when trying to build a package that already exists
# USAGE: information_package_already_exists $file
information_package_already_exists() {
	local file
	file="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='%s existe déjà.\n'
		;;
		('en'|*)
			message='%s already exists.\n'
		;;
	esac
	print_message 'info' "$message" \
		 "$file"
}

# print package building message
# USAGE: information_package_building $file
information_package_building() {
	local file
	file="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Construction de %s…\n'
		;;
		('en'|*)
			message='Building %s…\n'
		;;
	esac
	print_message 'info' "$message" \
		"$file"
}

# Error - The provided package id uses an invalid format
# USAGE: error_package_id_invalid $package_id
error_package_id_invalid() {
	local package_id
	package_id="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Lʼid de paquet fourni ne correspond pas au format attendu : "%s"\n'
			message="$message"'Cette valeur ne peut utiliser que des caractères du set [-a-z0-9],'
			message="$message"' et ne peut ni débuter ni sʼachever par un tiret.\n'
		;;
		('en'|*)
			message='The provided package id is not using the expected format: "%s"\n'
			message="$message"'The value should only include characters from the set [-a-z0-9],'
			message="$message"' and can not begin nor end with an hyphen.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$package_id"
}

# Error - The generation of the given package failed.
# USAGE: error_package_generation_failed $package_name
error_package_generation_failed() {
	local package_name
	package_name="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La génération du paquet suivant a échoué : %s\n'
			message="$message"'Merci de signaler cet échec sur notre système de suivi : %s\n\n'
		;;
		('en'|*)
			message='The generation of the following package failed: %s\n'
			message="$message"'Please report this error on our bugs tracker: %s\n\n'
		;;
	esac
	print_message 'error' "$message" \
		"$package_name" \
		"$PLAYIT_BUG_TRACKER_URL"
}

# Error - The generation of the metadata for the given package failed.
# USAGE: error_package_metadata_generation_failed $package_name
error_package_metadata_generation_failed() {
	local package_name
	package_name="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La génération des méta-données du paquet suivant a échoué : %s\n'
		;;
		('en'|*)
			message='The generation of the following package metadata failed: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$package_name"
}

