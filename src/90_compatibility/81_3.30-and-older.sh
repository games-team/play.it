# Keep compatibility with 3.30 and older

check_deps() {
	if compatibility_level_is_at_least '3.31'; then
		warning_deprecated_function 'check_deps' 'requirements_check'
	fi

	requirements_check
}

requirements_list_explicit_legacy() {
	if compatibility_level_is_at_least '3.31'; then
		warning_deprecated_variable 'SCRIPT_DEPS' 'REQUIREMENTS_LIST'
	fi

	printf '%s\n' ${SCRIPT_DEPS:-}
}

