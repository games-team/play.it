# Keep compatibility with 2.27 and older

icons_inclusion() {
	if compatibility_level_is_at_least '2.28'; then
		warning_deprecated_function 'icons_inclusion' 'content_inclusion_icons'
	fi

	local package
	package=$(current_package)
	content_inclusion_icons "$package" "$@"
}

launchers_write() {
	if compatibility_level_is_at_least '2.28'; then
		warning_deprecated_function 'launchers_write' 'launchers_generation'
	fi

	local package
	package=$(current_package)
	launchers_generation "$package" "$@"
}

