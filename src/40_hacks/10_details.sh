# Print the name of the given hack
# This is used to compute the file names of the source and the generated library.
# USAGE: hack_name $hack
# RETURN: the hack name, on a single line,
#         throw an error if it spans multiple lines,
#         throw an error if no name is set
hack_name() {
	local hack
	hack="$1"

	local hack_name
	hack_name=$(context_value "${hack}_NAME")

	# Throw an error if the name is empty
	if [ -z "$hack_name" ]; then
		error_missing_variable "${hack}_NAME"
		return 1
	fi

	# Throw an error if the name includes line breaks
	local line_breaks_number
	line_breaks_number=$(printf '%s' "$hack_name" | wc --lines)
	if [ "$line_breaks_number" -gt 0 ]; then
		error_variable_multiline "${hack}_NAME"
		return 1
	fi

	printf '%s' "$hack_name"
}

# Print the description of the given hack
# USAGE: hack_description $hack
# RETURN: the hack description, it can span over multiple lines,
#         throw an error if no description is set
hack_description() {
	local hack
	hack="$1"

	local hack_description
	hack_description=$(context_value "${hack}_DESCRIPTION")

	# Throw an error if the description is empty
	if [ -z "$hack_description" ]; then
		error_missing_variable "${hack}_DESCRIPTION"
		return 1
	fi

	printf '%s' "$hack_description"
}

# Print the identifier of the package that should inclide the given hack
# USAGE: hack_package $hack
# RETURN: a package identifier, falling back on the current package
hack_package() {
	local hack
	hack="$1"

	local hack_package
	hack_package=$(context_value "${hack}_PACKAGE")

	# Fall back on the current package identifier
	if [ -z "$hack_package" ]; then
		hack_package=$(current_package)
	fi

	printf '%s' "$hack_package"
}

# Print the content of the hack source
# USAGE: hack_source $hack
# RETURN: the hack source, usually spanning over multiple lines,
#         throw an error if no source is set
hack_source() {
	local hack
	hack="$1"

	local hack_source
	hack_source=$(context_value "${hack}_SOURCE")

	# Throw an error if the source is empty
	if [ -z "$hack_source" ]; then
		error_missing_variable "${hack}_SOURCE"
		return 1
	fi

	printf '%s' "$hack_source"
}

