# Error - The build of a preload shim failed
# USAGE: error_hack_build_failure
error_hack_build_failure() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La construction d’une bibliothèque depuis ses sources a échoué.\n'
			message="$message"'Ce message devrait être précédé d’une erreur indiquant des en-têtes de compilation manquantes.\n'
		;;
		('en'|*)
			message='The building of a library from its sources failed.\n'
			message="$message"'This message should be preceded with an error indicating the missing compilation headers.\n'
		;;
	esac
	print_message 'error' "$message"
}

