# List the LD_PRELOAD hacks that should be included for the current game
# USAGE: hacks_list
# RETURN: a list of hack identifiers, one per line,
#         or an empty string if the current game requires no hack
hacks_list() {
	local hacks_list
	hacks_list=$(context_value 'PRELOAD_HACKS_LIST')

	if [ -z "$hacks_list" ]; then
		return 0
	fi

	local hack
	for hack in $hacks_list; do
		printf '%s\n' "$hack"
	done
}

