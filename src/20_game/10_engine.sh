# Print the name of the engine used by the current game
# USAGE: game_engine
# RETURN: the game engine,
#         or an empty string if none is set
game_engine() {
	local game_engine
	game_engine="${GAME_ENGINE:-}"

	# Try to identify games using Unity3D
	if [ -z "$game_engine" ]; then
		local unity3d_name
		unity3d_name=$(unity3d_name)
		if [ -n "$unity3d_name" ]; then
			game_engine='unity3d'
		fi
	fi

	# Try to identify games using Unreal Engine 4
	if [ -z "$game_engine" ]; then
		local unrealengine4_name
		unrealengine4_name=$(unrealengine4_name)
		if [ -n "$unrealengine4_name" ]; then
			game_engine='unrealengine4'
		fi
	fi

	# Try to identify games using Visionaire
	if [ -z "$game_engine" ]; then
		local visionaire_name
		visionaire_name=$(visionaire_name)
		if [ -n "$visionaire_name" ]; then
			game_engine='visionaire'
		fi
	fi

	printf '%s' "$game_engine"
}

