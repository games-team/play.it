# Java launcher - Print the script content
# USAGE: java_launcher $application
java_launcher() {
	local application
	application="$1"

	local prefix_type
	prefix_type=$(application_prefix_type "$application")
	case "$prefix_type" in
		('symlinks')
			launcher_headers

			# Set the paths that should be available to the generated launcher
			launcher_init_paths "$application"

			java_launcher_environment "$application"

			# Generate the game prefix
			prefix_generate_links_farm
			launcher_prefix_symlinks_build

			# Set up the paths diversion to persistent storage
			persistent_storage_initialization
			persistent_storage_common
			persistent_path_diversion
			persistent_storage_update_directories
			persistent_storage_update_files

			java_launcher_run "$application"

			# Update persistent storage with files from the current prefix
			persistent_storage_update_files_from_prefix

			launcher_exit
		;;
		(*)
			error_launchers_prefix_type_unsupported "$application"
			return 1
		;;
	esac
}

# Java launcher - Set the environment
# USAGE: java_launcher_environment $application
java_launcher_environment() {
	local application
	application="$1"

	local application_exe
	application_exe=$(application_exe_escaped "$application")

	cat <<- EOF
	# Set the environment

	APP_EXE='$application_exe'

	EOF
}

# Java launcher - Run Java
# USAGE: java_launcher_run $application
java_launcher_run() {
	local application
	application="$1"

	cat <<- 'EOF'
	# Run the game

	cd "$PATH_PREFIX"

	EOF

	# Set the preload paths for native libraries
	cat <<- 'EOF'
	## Set the preload paths for native libraries
	export LD_LIBRARY_PATH="${PATH_LIBRARIES_USER}:${PATH_LIBRARIES_SYSTEM}:${LD_LIBRARY_PATH:-}"

	EOF

	application_prerun "$application"

	cat <<- 'EOF'
	## Do not exit on application failure,
	## to ensure post-run commands are run.
	set +o errexit

	EOF
	game_exec_line "$application"
	cat <<- 'EOF'

	game_exit_status=$?
	set -o errexit

	EOF

	application_postrun "$application"
}

# Java - Print the line starting the game
# USAGE: java_exec_line $application
# RETURN: the command to execute, including its command line options
java_exec_line() {
	local application
	application="$1"

	local application_java_options application_options
	application_java_options=$(application_java_options "$application")
	application_options=$(application_options "$application")
	cat <<- EOF
	java $application_java_options -jar "\$APP_EXE" $application_options "\$@"
	EOF
}

