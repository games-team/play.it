# Print the context-sensitive value for the given variable
# Context priority order is the following one:
# - archive-specific
# - package-specific
# - default
# - empty
# USAGE: context_value $variable_name
# RETURN: the context-sensitive value of the given variable,
#         or an empty string
context_value() {
	local variable_name
	variable_name="$1"

	local context_name
	context_name=$(context_name "$variable_name")
	# Return early if this variable has no set value.
	if [ -z "$context_name" ]; then
		return 0
	fi

	get_value "$context_name"
}

