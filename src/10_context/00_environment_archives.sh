# Set the identifier of the current archive
# USAGE: set_current_archive $archive
set_current_archive() {
	local archive
	archive="$1"

	# Check that the identifier uses the expected ARCHIVE_BASE_xxx format.
	local regexp
	regexp='^ARCHIVE_BASE\(_[0-9A-Z]\+\)*_[0-9]\+$'
	if ! printf '%s' "$archive" | grep --quiet --regexp="$regexp"; then
		error_current_archive_format_invalid "$archive"
		## exit is used instead of return to ensure a failure when called from a subshell.
		exit 1
	fi

	export PLAYIT_CONTEXT_ARCHIVE="$archive"
}

# Print the identifier of the current archive.
# USAGE: current_archive
# RETURNS: the current archive identifier,
#          or an empty string if no archive is set
current_archive() {
	# To ensure backwards-compatibility, the legacy variable should have a higher priority than the modern one.
	# Otherwise the ability to set the context using $ARCHIVE from game scripts would be lost as soon as the library calls set_current_archive.
	local archive
	archive="${ARCHIVE:-}"
	if
		[ -n "$archive" ] &&
		compatibility_level_is_at_least '2.27'
	then
		warning_context_legacy_archive
	fi

	if [ -z "$archive" ]; then
		archive="${PLAYIT_CONTEXT_ARCHIVE:-}"
	fi

	printf '%s' "$archive"
}

