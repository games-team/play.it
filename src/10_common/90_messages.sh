# Information - All the packages are already built, there is no need to run a new build
# USAGE: info_all_packages_already_built
info_all_packages_already_built() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Tous les paquets sont déjà présents, il nʼy a pas besoin de les reconstruire.\n'
		;;
		('en'|*)
			message='All the packages are already built, there is no need to run a new build.\n'
		;;
	esac

	print_message 'info' "$message"
}

# Error - The given path is not a file
# USAGE: error_not_a_file $path
error_not_a_file() {
	local path
	path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='"%s" nʼest pas un fichier valide.\n'
		;;
		('en'|*)
			message='"%s" is not a valid file.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$path"
}

# Error - The available tar implementation is not supported
# USAGE: error_unknown_tar_implementation
error_unknown_tar_implementation() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La version de tar présente sur ce système nʼest pas reconnue.\n'
			message="$message"'./play.it ne peut utiliser que GNU tar ou bsdtar.\n'
			message="$message"'Merci de signaler cette erreur sur notre outil de gestion de bugs : %s\n'
		;;
		('en'|*)
			message='The tar implementation on this system wasnʼt recognized.\n'
			message="$message"'./play.it can only use GNU tar or bsdtar.\n'
			message="$message"'Please report this issue in our bug tracker: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$PLAYIT_BUG_TRACKER_URL"
}

# Error - The given path is not a directory
# USAGE: error_not_a_directory $path
error_not_a_directory() {
	local path
	path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='"%s" nʼest pas un répertoire.\n'
		;;
		('en'|*)
			message='"%s" is not a directory.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$path"
}

# Error - The given path is not writable
# USAGE: error_not_writable $path
error_not_writable() {
	local path
	path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='"%s" nʼest pas accessible en écriture.\n'
		;;
		('en'|*)
			message='"%s" is not writable.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$path"
}

# Error - A required command is missing
# USAGE: error_unavailable_command $function $required_command
error_unavailable_command() {
	local function required_command
	function="$1"
	required_command="$2"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La commande "%s" nʼest pas disponible, mais elle est requise par la fonction "%s".\n'
			message="$message"'Merci de signaler cette erreur sur notre outil de gestion de bugs : %s\n'
		;;
		('en'|*)
			message='"%s" command is not available, but it is required by function "%s".\n'
			message="$message"'Please report this issue in our bug tracker: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$required_command" \
		"$function" \
		"$PLAYIT_BUG_TRACKER_URL"
}

# Error - The calling script is not compatible with the provided library version
# USAGE: error_incompatible_versions
error_incompatible_versions() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Ce script nʼest pas compatible avec la version fournie de la bibliothèque ./play.it.\n'
			message="$message"'La bibliothèque utilisée fournit actuellement la version %s, mais le script attend une version ≥ %s et < %s.\n'
		;;
		('en'|*)
			message='This script is not compatible with the provided ./play.it library version.\n'
			message="$message"'The library in use currently provides version %s, but the script expects a version ≥ %s and < %s.\n'
		;;
	esac

	local compatibility_level version_major_minimum
	compatibility_level=$(compatibility_level)
	version_major_minimum=$(printf '%s' "$compatibility_level" | cut --delimiter='.' --fields=1)
	print_message 'error' "$message" \
			"$LIBRARY_VERSION" \
			"$compatibility_level" \
			"$((version_major_minimum + 1)).0"
}

# Error - The wrapper has been called with no archive argument
# USAGE: error_archive_missing_from_arguments
error_archive_missing_from_arguments() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Aucune archive nʼa été fournie sur la ligne de commande.\n'
		;;
		('en'|*)
			message='No archive has been provided on the command line.\n'
		;;
	esac
	print_message 'error' "$message"
}

# Error - No game script has been found for the given archive
# USAGE: error_no_script_found_for_archive $archive
error_no_script_found_for_archive() {
	local archive
	archive="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Impossible de trouver un script pour le fichier %s\n'
		;;
		('en'|*)
			message='Could not find script for file %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive"
}

# Error - A variable is spanning multiple lines
# USAGE: error_variable_multiline $variable_name
error_variable_multiline() {
	local variable_name
	variable_name="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La valeur %s sʼétend sur plusieurs lignes, ce qui nʼest pas autorisé.\n'
		;;
		('en')
			message='%s value is spanning multiple lines, but this is not allowed.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$variable_name"
}

# Error - A type-restricted function has been called on the wrong application type
# USAGE: error_application_wrong_type $function_name $application_type
error_application_wrong_type() {
	local function_name application_type
	function_name="$1"
	application_type="$2"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='%s ne peut pas être appelée sur les applications utilisant le type "%s".\n'
		;;
		('en')
			message='%s can not be called on applications using "%s" type.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$function_name" \
		"$application_type"
}

# Error - The directory for temporary files storage does not exist
# USAGE: error_temporary_path_not_a_directory $temporary_directory_path
error_temporary_path_not_a_directory() {
	local temporary_directory_path
	temporary_directory_path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin demandé pour stocker les fichiers temporaires nʼexiste pas'
			message="$message"', ou nʼest pas un répertoire : %s\n'
			message="$message"'Un chemin alternatif peut être fourni avec --tmpdir.\n'
		;;
		('en'|*)
			message='The path set for temporary files storage does not exist'
			message="$message"', or is not a directory: %s\n'
			message="$message"'An alternative path can be provided with --tmpdir.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$temporary_directory_path"
}

# Error - The directory for temporary files storage has no write access
# USAGE: error_temporary_path_not_writable $temporary_directory_path
error_temporary_path_not_writable() {
	local temporary_directory_path
	temporary_directory_path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin demandé pour stocker les fichiers temporaires nʼest pas accessible en écriture : %s\n'
			message="$message"'Un chemin alternatif peut être fourni avec --tmpdir.\n'
		;;
		('en'|*)
			message='The path set for temporary files storage has no write access: %s\n'
			message="$message"'An alternative path can be provided with --tmpdir.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$temporary_directory_path"
}

# Error - The directory for temporary files storage is not case-sensitive
# USAGE: error_temporary_path_not_case_sensitive $temporary_directory_path
error_temporary_path_not_case_sensitive() {
	local temporary_directory_path
	temporary_directory_path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin demandé pour stocker les fichiers temporaires est sur un système de fichiers qui nʼest pas sensible à la casse des noms de fichiers : %s\n'
			message="$message"'Un chemin alternatif peut être fourni avec --tmpdir.\n'
		;;
		('en'|*)
			message='The path set for temporary files storage is on a case-insensitive file system: %s\n'
			message="$message"'An alternative path can be provided with --tmpdir.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$temporary_directory_path"
}

# Error - The directory for temporary files storage has no support for UNIX permissions
# USAGE: error_temporary_path_no_unix_permissions $temporary_directory_path
error_temporary_path_no_unix_permissions() {
	local temporary_directory_path
	temporary_directory_path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin demandé pour stocker les fichiers temporaires ne prend pas en charge les permissions UNIX : %s\n'
			message="$message"'Un chemin alternatif peut être fourni avec --tmpdir.\n'
		;;
		('en'|*)
			message='The path set for temporary files storage has no support for UNIX permissions: %s\n'
			message="$message"'An alternative path can be provided with --tmpdir.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$temporary_directory_path"
}

# Error - The directory for temporary files storage is mounted with noexec
# USAGE: error_temporary_path_noexec $temporary_directory_path
error_temporary_path_noexec() {
	local temporary_directory_path
	temporary_directory_path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin demandé pour stocker les fichiers temporaires ne permet pas la création de fichiers exécutables : %s\n'
			message="$message"'Un chemin alternatif peut être fourni avec --tmpdir.\n'
		;;
		('en'|*)
			message='The path set for temporary files storage forbid the creation of executable files: %s\n'
			message="$message"'An alternative path can be provided with --tmpdir.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$temporary_directory_path"
}

# Error - There is not enough free space in the directory for temporary files storage
# USAGE: error_temporary_path_not_enough_space $temporary_directory_path
error_temporary_path_not_enough_space() {
	local temporary_directory_path
	temporary_directory_path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin demandé pour stocker les fichiers temporaires ne dispose pas dʼassez dʼespace libre : %s\n'
			message="$message"'Un chemin alternatif peut être fourni avec --tmpdir.\n'
			message="$message"'Cette vérification de lʼespace libre peut aussi être contournée avec --no-free-space-check.\n'
		;;
		('en'|*)
			message='The path set for temporary files storage has not enough free space: %s\n'
			message="$message"'An alternative path can be provided with --tmpdir.\n'
			message="$message"'This free space check can also be disabled using --no-free-space-check.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$temporary_directory_path"
}

# Error - A mandatory variable is not set
# USAGE: error_missing_variable $variable_name
error_missing_variable() {
	local variable_name
	variable_name="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La variable suivante est requise, mais elle nʼa pas été définie ou sa valeur est nulle : %s\n'
		;;
		('en'|*)
			message='The following variable is mandatory, but it is unset or its value is null: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$variable_name"
}

# Error - ./play.it should not be run with the root account
# USAGE: error_run_as_root
error_run_as_root() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='./play.it ne doit pas être exécuté par le compte root.\n'
		;;
		('en'|*)
			message='./play.it should not be executed by the root account.\n'
		;;
	esac
	print_message 'error' "$message"
}

# Error - The current archive identifier uses an invalid format.
# USAGE: error_current_archive_format_invalid $archive
error_current_archive_format_invalid() {
	local archive
	archive="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Lʼidentifiant dʼarchive "%s" ne respecte pas le format attendu: ARCHIVE_BASE_xxx\n'
		;;
		('en'|*)
			message='The archive identifier "%s" does not follow the expected format: ARCHIVE_BASE_xxx\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive"
}

# Error - The current package identifier uses an invalid format.
# USAGE: error_current_package_format_invalid $package
error_current_package_format_invalid() {
	local package
	package="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Lʼidentifiant de paquet "%s" ne respecte pas le format attendu: PKG_xxx\n'
		;;
		('en'|*)
			message='The package identifier "%s" does not follow the expected format: PKG_xxx\n'
		;;
	esac
	print_message 'error' "$message" \
		"$package"
}

# Error - The current package identifier is not included in the list of packages to build.
# USAGE: error_current_package_not_in_list $package
error_current_package_not_in_list() {
	local package
	package="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Lʼidentifiant de paquet "%s" ne fait pas partie de la liste de paquets à construire.\n'
		;;
		('en'|*)
			message='The package identifier "%s" is not included in the list of packages that should be built.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$package"
}

# Error - The compatibility level of the current game script is set to an invalid value
# USAGE: error_invalid_compatibility_level $compatibility_level
error_invalid_compatibility_level() {
	local compatibility_level
	compatibility_level="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le niveau de compatibilité du script courant est défini à une valeur invalide : "%s"\n'
			message="$message"'Le format attendu est "version_majeure.version_mineure", par exemple "2.29".\n'
		;;
		('en'|*)
			message='The compatibility level of the current script is set to an invalid value: "%s"\n'
			message="$message"'The expected format is "major_version.minor_version", for example "2.29".\n'
		;;
	esac
	print_message 'error' "$message" \
		"$compatibility_level"
}

# Error - The working directory is not set, but its removal has been required
# USAGE: error_working_directory_unset
error_working_directory_unset() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin vers le répertoire de travail temporaire n’est pas encore défini, mais sa suppression a déjà été demandée.\n'
		;;
		('en'|*)
			message='The path to the temporary working directory is not set yet, but its removal has already been required.\n'
		;;
	esac
	print_message 'error' "$message"
}

