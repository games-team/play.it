# Clean-up the output of a command returning a list
# USAGE: … | list_clean
# RETURN: a list including only unique lines
list_clean() {
	## - remove leading and trailing spaces
	## - sort the list and merge duplicate entries
	## - remove empty lines, ignore errors on empty output
	sed 's/^\s*//;s/\s*$//' |
		sort --unique |
		grep --invert-match --regexp='^$' || true
}

# Clean-up the output of a command printing a snippet
# USAGE: … | snippet_clean
# RETURN: a snippet indented with tabulations
snippet_clean() {
	## - convert the series of 4 spaces to tabulations
	sed --regexp-extended 's/( ){4}/\t/g'
}

