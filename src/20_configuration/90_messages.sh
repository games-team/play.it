# Error - An invalid value has been provided for the given option
# USAGE: error_option_invalid $option_name $option_value
error_option_invalid() {
	local option_name option_value
	option_name="$1"
	option_value="$2"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='"%s" nʼest pas une valeur valide pour --%s.\n'
		;;
		('en'|*)
			message='"%s" is not a valid value for --%s.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$option_value" \
		"$option_name"
}

# Error - The configuration file could not be found
# USAGE: error_config_file_not_found $config_file_path
error_config_file_not_found() {
	local config_file_path
	config_file_path="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le fichier de configuration %s nʼa pas pu être trouvé.\n'
			;;
		('en'|*)
			message='The configuration file %s has not been found.\n'
			;;
	esac
	print_message 'error' "$message" \
		"$config_file_path"
}

# Error - Some options are currently set to incompatible values
# USAGE: error_incompatible_options $option_name_1 $option_name_2
error_incompatible_options() {
	local option_name_1 option_name_2
	option_name_1="$1"
	option_name_2="$2"

	local option_value_1 option_value_2
	option_value_1=$(option_value "$option_name_1")
	option_value_2=$(option_value "$option_name_2")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Les options suivantes ne sont pas compatibles :\n'
			message="$message"'\t--%s %s\n'
			message="$message"'\t--%s %s\n\n'
		;;
		('en'|*)
			message='The following options are not compatible:\n'
			message="$message"'\t--%s %s\n'
			message="$message"'\t--%s %s\n\n'
		;;
	esac
	print_message 'error' "$message" \
		"$option_name_1" \
		"$option_value_1" \
		"$option_name_2" \
		"$option_value_2"
}

