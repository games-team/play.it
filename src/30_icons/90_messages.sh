# Error - An icon file could not be found
# USAGE: error_icon_file_not_found $file
error_icon_file_not_found() {
	local file
	file="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le fichier dʼicône suivant est introuvable : %s\n'
			message="$message"'Merci de signaler cette erreur sur notre outil de gestion de bugs : %s\n'
		;;
		('en'|*)
			message='The following icon file could not be found: %s\n'
			message="$message"'Please report this issue in our bug tracker: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$file" \
		"$PLAYIT_GAMES_BUG_TRACKER_URL"
}

# Error - The path to the given icon is not set
# USAGE: error_icon_path_empty $icon
error_icon_path_empty() {
	local icon
	icon="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='%s nʼest pas défini, mais il y a eu une tentative de récupérer le chemin de cette icône.\n'
		;;
		('en'|*)
			message='%s is not set, but there has been a request for this icon path.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$icon"
}

# Error - An icon file with an unsupported MIME type has been provided
# USAGE: error_icon_unsupported_type $icon_file $icon_type
error_icon_unsupported_type() {
	local icon_file icon_type
	icon_file="$1"
	icon_type="$2"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le fichier dʼicône suivant est du type MIME "%s", qui nʼest pas pris en charge : %s\n'
		;;
		('en'|*)
			message='The following icon file is of the "%s" MIME type, that is not supported: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$icon_type" \
		"$icon_file"
}

# Error - No application identifier could be found related to the given icon identifier
# USAGE: error_icon_application_not_found $icon_identifier
error_icon_application_not_found() {
	local icon_identifier
	icon_identifier="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='L’identifiant d’icône fourni ne semble pas correspondre à une des applications prises en charge : %s\n'
		;;
		('en'|*)
			message='The given icon identifier does not seem to related to any of the supported applications: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$icon_identifier"
}

# Error - No .ico file has been extracted from the given .exe file
# USAGE: error_no_ico_file_extracted $icon_identifier
error_no_ico_file_extracted() {
	local icon_identifier
	icon_identifier="$1"

	local icon_file wrestool_options
	icon_file=$(icon_full_path "$icon_identifier")
	wrestool_options=$(icon_wrestool_options "$icon_identifier")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Aucun fichier d’icône n’a été extrait du fichier suivant : "%s"\n'
			message="$message"'Les options suivantes ont été passées à wrestool : %s\n'
		;;
		('en'|*)
			message='No icon file has been extracted from the following file: "%s"\n'
			message="$message"'The following options have been passed to wrestool: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$icon_file" \
		"$wrestool_options"
}

# Error - No icon is set for the given application
# USAGE: error_no_icon_found $application
error_no_icon_found() {
	local application
	application="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Aucune icône n’est définie pour l’application suivante : %s\n'
		;;
		('en'|*)
			message='No icon is set for the following application: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$application"
}

