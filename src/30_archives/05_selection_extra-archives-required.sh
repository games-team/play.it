# Check for the presence of required extra archives
# USAGE: archives_required_extra_presence_check
# RETURN: 0 if no extra archive is required, or all required archives are found,
#         1 if a required archive is missing
archives_required_extra_presence_check() {
	# Check the presence of archives providing required native libraries
	local libraries_required library_required presence_check_function
	libraries_required=$(dependencies_list_native_libraries_all)
	while read -r library_required; do
		case "$library_required" in
			('libcurl.so.4+CURL_OPENSSL_3')
				presence_check_function='archive_required_extra_presence_check_libcurl3'
			;;
			('libFLAC.so.8')
				presence_check_function='archive_required_extra_presence_check_libflac8'
			;;
			('libgconf-2.so.4')
				presence_check_function='archive_required_extra_presence_check_libgconf2'
			;;
			('libidn.so.11')
				presence_check_function='archive_required_extra_presence_check_libidn11'
			;;
			('libpng12.so.0')
				presence_check_function='archive_required_extra_presence_check_libpng12'
			;;
			('libssl.so.1.0.0')
				presence_check_function='archive_required_extra_presence_check_libssl100'
			;;
			('libssl.so.1.1')
				presence_check_function='archive_required_extra_presence_check_libssl11'
			;;
		esac
	done <<- EOL
	$(printf '%s' "$libraries_required")
	EOL

	# WARNING: This function can be called through a test,
	#          preventing the ability to rely on set -o errexit
	if [ -n "${presence_check_function:-}" ]; then
		if ! $presence_check_function; then
			return 1
		fi
	fi

	# Check the presence of archives required to apply tweaks on the WINE prefix
	local wineprefix_tweaks wineprefix_tweak
	wineprefix_tweaks=$(wine_wineprefix_tweaks)
	while read -r wineprefix_tweak; do
		case "$wineprefix_tweak" in
			('mono')
				# WARNING: This function can be called through a test,
				#          preventing the ability to rely on set -o errexit
				if ! archive_required_extra_presence_check_mono; then
					return 1
				fi
			;;
		esac
	done <<- EOL
	$(printf '%s' "$wineprefix_tweaks")
	EOL
}

# Check for the presence of the extra archive providing libcurl.so.3 and libcurl.so.4 including the CURL_OPENSSL_3 symbol
# USAGE: archive_required_extra_presence_check_libcurl3
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_libcurl3() {
	ARCHIVE_REQUIRED_LIBCURL3_NAME='curl_7.52.1.tar.xz'
	ARCHIVE_REQUIRED_LIBCURL3_MD5='ae0368de97368164801618a08c70cb34'
	ARCHIVE_REQUIRED_LIBCURL3_URL='https://downloads.dotslashplay.it/resources/curl/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_LIBCURL3_NAME ARCHIVE_REQUIRED_LIBCURL3_MD5 ARCHIVE_REQUIRED_LIBCURL3_URL

	archive_initialize_required \
		'ARCHIVE_LIBCURL3' \
		'ARCHIVE_REQUIRED_LIBCURL3'
}

# Check for the presence of the extra archive providing libFLAC.so.8
# USAGE: archive_required_extra_presence_check_libflac8
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_libflac8() {
	# On Arch Linux, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			return 0
		;;
	esac

	ARCHIVE_REQUIRED_LIBFLAC8_NAME='libflac8.tar.xz'
	ARCHIVE_REQUIRED_LIBFLAC8_MD5='1f0d785f52474cb2232a2f8f8b561eda'
	ARCHIVE_REQUIRED_LIBFLAC8_URL='https://downloads.dotslashplay.it/resources/flac/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_LIBFLAC8_NAME ARCHIVE_REQUIRED_LIBFLAC8_MD5 ARCHIVE_REQUIRED_LIBFLAC8_URL

	archive_initialize_required \
		'ARCHIVE_LIBFLAC8' \
		'ARCHIVE_REQUIRED_LIBFLAC8'
}

# Check for the presence of the extra archive providing GConf 2 library
# USAGE: archive_required_extra_presence_check_libgconf2
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_libgconf2() {
	# On Arch Linux, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			return 0
		;;
	esac

	ARCHIVE_REQUIRED_LIBGCONF2_NAME='libgconf-2-4.tar.xz'
	ARCHIVE_REQUIRED_LIBGCONF2_MD5='4ae540fd4114ee2ddd7bd841017aad3b'
	ARCHIVE_REQUIRED_LIBGCONF2_URL='https://downloads.dotslashplay.it/resources/gconf/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_LIBGCONF2_NAME ARCHIVE_REQUIRED_LIBGCONF2_MD5 ARCHIVE_REQUIRED_LIBGCONF2_URL

	archive_initialize_required \
		'ARCHIVE_LIBGCONF2' \
		'ARCHIVE_REQUIRED_LIBGCONF2'
}

# Check for the presence of the extra archive providing GNU Libidn 11 library
# USAGE: archive_required_extra_presence_check_libidn11
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_libidn11() {
	# On Arch Linux, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			return 0
		;;
	esac

	ARCHIVE_REQUIRED_LIBIDN11_NAME='libidn11_1.33.tar.xz'
	ARCHIVE_REQUIRED_LIBIDN11_MD5='75d95702ec6a2b327c8902cc14998926'
	ARCHIVE_REQUIRED_LIBIDN11_URL='https://downloads.dotslashplay.it/resources/libidn/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_LIBIDN11_NAME ARCHIVE_REQUIRED_LIBIDN11_MD5 ARCHIVE_REQUIRED_LIBIDN11_URL

	archive_initialize_required \
		'ARCHIVE_LIBIDN11' \
		'ARCHIVE_REQUIRED_LIBIDN11'
}

# Check for the presence of the extra archive providing PNG 1.2 libraries
# USAGE: archive_required_extra_presence_check_libpng12
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_libpng12() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	ARCHIVE_REQUIRED_LIBPNG12_NAME='libpng_1.2.tar.xz'
	ARCHIVE_REQUIRED_LIBPNG12_MD5='121c92152cce69f589a6d66a1e613bdb'
	ARCHIVE_REQUIRED_LIBPNG12_URL='https://downloads.dotslashplay.it/resources/libpng/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_LIBPNG12_NAME ARCHIVE_REQUIRED_LIBPNG12_MD5 ARCHIVE_REQUIRED_LIBPNG12_URL

	archive_initialize_required \
		'ARCHIVE_LIBPNG12' \
		'ARCHIVE_REQUIRED_LIBPNG12'
}

# Check for the presence of the extra archive providing OpenSSL 1.0.0 libraries
# USAGE: archive_required_extra_presence_check_libssl100
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_libssl100() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	ARCHIVE_REQUIRED_OPENSSL100_NAME='openssl_1.0.0.tar.xz'
	ARCHIVE_REQUIRED_OPENSSL100_MD5='9822e4dd8cb467dad843044c3135b5c5'
	ARCHIVE_REQUIRED_OPENSSL100_URL='https://downloads.dotslashplay.it/resources/openssl/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_OPENSSL100_NAME ARCHIVE_REQUIRED_OPENSSL100_MD5 ARCHIVE_REQUIRED_OPENSSL100_URL

	archive_initialize_required \
		'ARCHIVE_OPENSSL100' \
		'ARCHIVE_REQUIRED_OPENSSL100'
}

# Check for the presence of the extra archive providing OpenSSL 1.1 libraries
# USAGE: archive_required_extra_presence_check_libssl11
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_libssl11() {
	# On Arch Linux and Gentoo, this library is still provided in the packages repositories
	local option_package
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch'|'egentoo'|'gentoo')
			return 0
		;;
	esac

	ARCHIVE_REQUIRED_OPENSSL11_NAME='openssl_1.1.1n.tar.xz'
	ARCHIVE_REQUIRED_OPENSSL11_MD5='dade7a54b213be8ac6e0bc2b571570cc'
	ARCHIVE_REQUIRED_OPENSSL11_URL='https://downloads.dotslashplay.it/resources/openssl/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_OPENSSL11_NAME ARCHIVE_REQUIRED_OPENSSL11_MD5 ARCHIVE_REQUIRED_OPENSSL11_URL

	archive_initialize_required \
		'ARCHIVE_OPENSSL11' \
		'ARCHIVE_REQUIRED_OPENSSL11'
}

# Check for the presence of the extra archive providing Mono, for inclusion in WINE prefixes
# USAGE: archive_required_extra_presence_check_mono
# RETURN: 0 if the required archive is found,
#         1 if it is missing
archive_required_extra_presence_check_mono() {
	ARCHIVE_REQUIRED_MONO_NAME='wine-mono-8.0.0-x86.msi'
	ARCHIVE_REQUIRED_MONO_MD5='4fe5c683fcd9634c7f6571f252b3603c'
	ARCHIVE_REQUIRED_MONO_URL='https://dl.winehq.org/wine/wine-mono/8.0.0/'
	## The archive properties will be reused later, for checking its integrity.
	export ARCHIVE_REQUIRED_MONO_NAME ARCHIVE_REQUIRED_MONO_MD5 ARCHIVE_REQUIRED_MONO_URL

	archive_initialize_required \
		'ARCHIVE_MONO' \
		'ARCHIVE_REQUIRED_MONO'
}

