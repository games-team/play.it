# display the name of a file currently processed
# USAGE: information_file_in_use $file
information_file_in_use() {
	local file
	file="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Utilisation de %s\n'
		;;
		('en'|*)
			message='Using %s\n'
		;;
	esac
	print_message 'info' "$message"
}

# print data extraction message
# USAGE: information_archive_data_extraction $file
information_archive_data_extraction() {
	local file
	file="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Extraction des données de %s…\n'
		;;
		('en'|*)
			message='Extracting data from %s…\n'
		;;
	esac
	print_message 'info' "$message" \
		"$file"
}

# print hash computation message
# USAGE: info_archive_hash_computation $file_path
info_archive_hash_computation() {
	local file_path
	file_path="$1"

	local file_name
	file_name=$(basename "$file_path")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Calcul de la somme de contrôle de %s…\n'
		;;
		('en'|*)
			message='Computing hashsum for %s…\n'
		;;
	esac
	print_message 'info' "$message" \
		"$file_name"
}

# display a list of archives, one per line, with their download URL if one is provided
# USAGE: information_archives_list $archive[…]
information_archives_list() {
	## TODO: The archive URL should be fetched using a dedicated function.
	local archive archive_name archive_url
	for archive in "$@"; do
		archive_name=$(archive_name "$archive")
		archive_url=$(get_value "${archive}_URL")
		if [ -n "$archive_url" ]; then
			print_message 'info' '%s — %s\n' \
				"$archive_name" \
				"$archive_url"
		else
			print_message 'info' '%s\n' \
				"$archive_name"
		fi
	done
}

# Warning - An optional icons archive is supported, but not available
# USAGE: warning_optional_archive_missing_icons $archive
warning_optional_archive_missing_icons() {
	local archive
	archive="$1"

	## TODO: The archive URL should be fetched using a dedicated function.
	local archive_name archive_url
	archive_name=$(archive_name "$archive")
	archive_url=$(get_value "${archive}_URL")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Une archive proposant des icônes pour ce jeu est disponible, mais n’a pas été trouvée : %s\n'
			message="$message"'Elle peut être téléchargée depuis l’URL suivante : %s\n\n'
		;;
		('en'|*)
			message='An archive providing icons for this game is available, but could not be found: %s\n'
			message="$message"'It can be downloaded from the following URL: %s\n\n'
		;;
	esac
	print_message 'warning' "$message" \
		"$archive_name" \
		"$archive_url"
}

# Error - No archive supported for the current game script
error_no_archive_supported() {
	local game_script
	game_script=$(realpath "$0")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Ce script semble ne prendre en charge aucune archive : %s\n'
			message="$message"'Merci de signaler cette erreur sur notre outil de suivi : %s\n'
		;;
		('en'|*)
			message='This script seems to support no archive: %s\n'
			message="$message"'Please report this issue in our bug tracker: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$game_script" \
		"$PLAYIT_GAMES_BUG_TRACKER_URL"
}

# Error - No extractor is available to handle the given archive
# USAGE: error_archive_no_extractor_found $archive_type
error_archive_no_extractor_found() {
	local archive_type
	archive_type="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Ce script a essayé dʼextraire le contenu dʼune archive de type "%s", mais aucun outil approprié nʼa été trouvé.\n'
			message="$message"'Merci de signaler cette erreur sur notre outil de gestion de bugs : %s\n'
		;;
		('en'|*)
			message='This script tried to extract the contents of a "%s" archive, but not appropriate tool could be found.\n'
			message="$message"'Please report this issue in our bug tracker: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive_type" \
		"$PLAYIT_GAMES_BUG_TRACKER_URL"
}

# Error - A required archive is not found
# List all the archives that could fulfill the requirement, with their download URL if one is provided
# USAGE: error_archive_not_found $archive[…]
error_archive_not_found() {
	# Get the path to the directory where the current archive is found
	local archives_path archives_path_full
	archives_path=$(archives_path_base)
	archives_path_full=$(realpath --no-symlinks "$archives_path")

	local messages_language message_1 message_2
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			if [ $# -eq 1 ]; then
				message_1='Le fichier suivant est introuvable dans %s :\n'
			else
				message_1='Aucun des fichiers suivants nʼest présent dans %s :\n'
			fi
			message_2='Vous devez télécharger le fichier requis avant de continuer.\n'
		;;
		('en'|*)
			if [ $# -eq 1 ]; then
				message_1='The following file could not be found in %s:\n'
			else
				message_1='None of the following files could be found in %s:\n'
			fi
			message_2='Please download the required file before proceeding.\n'
		;;
	esac
	print_message 'error' "$message_1" \
		"$archives_path_full"
	information_archives_list "$@" > /dev/stderr
	print_message 'info' "$message_2" > /dev/stderr
}

# Error - The type of the given archive could not be guessed
# USAGE: error_archive_type_not_set $archive
error_archive_type_not_set() {
	local archive
	archive="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='ARCHIVE_TYPE nʼest pas défini pour %s et nʼa pas pu être détecté automatiquement.\n'
		;;
		('en'|*)
			message='ARCHIVE_TYPE is not set for %s and could not be guessed.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive"
}

# Error - An integrity check failed
# USAGE: error_hashsum_mismatch $file_path
error_hashsum_mismatch() {
	local file_path
	file_path="$1"

	local file_name
	file_name=$(basename "$file_path")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Somme de contrôle incohérente. %s nʼest pas le fichier attendu.\n'
			message="$message"'Utilisez --checksum=none pour forcer son utilisation.\n'
		;;
		('en'|*)
			message='Hashsum mismatch. %s is not the expected file.\n'
			message="$message"'Use --checksum=none to force its use.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$file_name"
}

# Error - The available version of innoextract is too old
# USAGE: error_innoextract_version_too_old $archive
error_innoextract_version_too_old() {
	local archive
	archive="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La version de innoextract disponible sur ce système est trop ancienne pour extraire les données de lʼarchive suivante : %s\n'
			message="$message"'Des instructions de mise-à-jour sont proposées :\n'
			message="$message"'- pour Debian : %s\n'
			message="$message"'- pour Ubuntu : %s\n'
		;;
		('en'|*)
			message='Available innoextract version is too old to extract data from the following archive: %s\n'
			message="$message"'Update instructions are proposed:\n'
			message="$message"'- for Debian: %s\n'
			message="$message"'- for Ubuntu: %s\n'
		;;
	esac
	## TODO: The innoextract upgrade instructions should be available from the Debian / Ubuntu documentations,
	##       not from the ./play.it one.
	print_message 'error' "$message" \
		"$archive" \
		'https://forge.dotslashplay.it/play.it/doc/-/wikis/user/distributions/debian#available-innoextract-version-is-too-old' \
		'https://forge.dotslashplay.it/play.it/doc/-/wikis/user/distributions/ubuntu#innoextract-version-is-too-old'
}

# Error - Invalid value used for archive type
# USAGE: error_archive_type_invalid $archive_type
error_archive_type_invalid() {
	local archive_type
	archive_type="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La valeur suivante ne correspond pas à un type dʼarchive connu : "%s"\n'
		;;
		('en'|*)
			message='The following value is not a valid archive type: "%s"\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive_type"
}

# Error - Invalid value used for archive extractor
# USAGE: error_archive_extractor_invalid $archive_extractor
error_archive_extractor_invalid() {
	local archive_extractor
	archive_extractor="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La valeur suivante ne correspond pas à un extracteur dʼarchive connu : "%s"\n'
		;;
		('en'|*)
			message='The following value is not a valid archive extractor: "%s"\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive_extractor"
}

# Error - Archive data extraction failed
# USAGE: error_archive_extraction_failure $archive
error_archive_extraction_failure() {
	local archive
	archive="$1"

	local archive_name
	archive_name=$(archive_name "$archive")

	local log_file
	log_file=$(archive_extraction_log_path)

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Lʼextraction des données depuis lʼarchive suivante a échoué : %s\n'
			message="$message"'Vous pouvez obtenir plus de détails dans le fichier journal : %s\n'
		;;
		('en'|*)
			message='Data extraction from the following archive failed: %s\n'
			message="$message"'You can get more details from the following log file: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive_name" \
		"$log_file"
}

# Error - The header length could not be fetched from the given Makeself archive
# USAGE: error_makeself_fetching_header_length $archive_name
error_makeself_fetching_header_length() {
	local archive_name
	archive_name="$1"

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La récupération de la longueur de l’en-tête de l’archive a échoué.\n'
			message="$message"'Il s’agit peut-être d’une archive identifiée à tort comme un installateur Makeself.\n'
		;;
		('en'|*)
			message='Fetching the header length from the archive failed.\n'
			message="$message"'This might be an archive that has been wrongly identified as a Makeself installer.\n'
		;;
	esac
	print_message 'error' "$message" \
		"$archive_name"
}

# Error - archive_extraction_default has been called before setting the current archive
# USAGE: error_archive_extraction_default_missing_archive
error_archive_extraction_default_missing_archive() {
	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='La fonction archive_extraction_default a été appelée alors qu’aucune archive n’a été définie.\n'
			message="$message"'Une étape essentielle de l’initialisation n’a pas été effectuée correctement.\n'
		;;
		('en'|*)
			message='The archive_extraction_default function has been called but no archive has been set yet.\n'
			message="$message"'A required initialization step has not been run as it should.\n'
		;;
	esac
	print_message 'error' "$message"
}

