# Error - path_libraries_system has been called for a package with an unsupported architecture
# USAGE: error_path_libraries_system_unsupported_architecture $package
error_path_libraries_system_unsupported_architecture() {
	local package
	package="$1"

	local package_architecture
	package_architecture=$(package_architecture "$package")

	local messages_language message
	messages_language=$(messages_language)
	case "$messages_language" in
		('fr')
			message='Le chemin vers les bibliothèques du système a été demandé pour le paquet "%s",\n'
			message="$message"'mais ce paquet vise une architecture pour lequel ce chemin n’est pas connu : %s\n'
		;;
		('en'|*)
			message='The path to the system libraries has been required for the package "%s",\n'
			message="$message"'but this path is not known for a package using this architecture: %s\n'
		;;
	esac
	print_message 'error' "$message" \
		"$package" \
		"$package_architecture"
}

