# Print install path for binaries.
# USAGE: path_binaries
path_binaries() {
	local install_prefix target_system path_structure
	install_prefix=$(option_value 'prefix')
	target_system=$(option_value 'package')
	case "$target_system" in
		('deb')
			# Debian uses /usr/games as the default path for game-related binaries.
			path_structure='%s/games'
		;;
		(*)
			# Non-Debian systems use /usr/bin as the default path for all binaries,
			# including game-related ones.
			path_structure='%s/bin'
		;;
	esac
	## Silence ShellCheck false-positive
	## Don't use variables in the printf format string. Use printf "..%s.." "$foo".
	# shellcheck disable=SC2059
	printf "$path_structure" "$install_prefix"
}

# Print install path for XDG .desktop menu entries.
# USAGE: path_xdg_desktop
path_xdg_desktop() {
	# For convenience, XDG .desktop menu entries are always installed under the default install prefix.
	# If they could be installed under a custom path like /opt/${game_id},
	# they would not be picked up by applications menus without a manual intervention from the system administrator.
	printf '/usr/share/applications'
}

# Print install path for documentation files.
# USAGE: path_documentation
path_documentation() {
	local install_prefix
	install_prefix=$(option_value 'prefix')

	local option_package last_path_component
	option_package=$(option_value 'package')
	case "$option_package" in
		('deb'|'arch')
			last_path_component=$(game_id)
		;;
		('gentoo'|'egentoo')
			local game_id package_version
			game_id=$(game_id)
			package_version=$(package_version)
			last_path_component="${game_id}-${package_version}"
		;;
	esac

	printf '%s/share/doc/%s' "$install_prefix" "$last_path_component"
}

# Print install path for game files.
# USAGE: path_game_data
path_game_data() {
	local install_prefix game_id target_system path_structure
	install_prefix=$(option_value 'prefix')
	game_id=$(game_id)
	target_system=$(option_value 'package')
	case "$target_system" in
		('deb')
			# Debian uses /usr/share/games as the default path for game-related data files.
			path_structure='%s/share/games/%s'
		;;
		(*)
			# Non-Debian systems use /usr/share as the default path for all data files,
			# including game-related ones.
			path_structure='%s/share/%s'
		;;
	esac
	## Silence ShellCheck false-positive
	## Don't use variables in the printf format string. Use printf "..%s.." "$foo".
	# shellcheck disable=SC2059
	printf "$path_structure" "$install_prefix" "$game_id"
}

# Print install path for icons.
# USAGE: path_icons
path_icons() {
	# Icons are always installed under the default install prefix.
	# launcher_desktop (src/30_launchers/00_common.sh) expects the icon to be available under either /usr or /usr/local.
	printf '/usr/share/icons/hicolor'
}

# Print install path for native libraries.
# USAGE: path_libraries
path_libraries() {
	local install_prefix game_id
	install_prefix=$(option_value 'prefix')
	game_id=$(game_id)

	local path_structure target_system
	## Set default value
	path_structure='%s/lib/games/%s'
	## Override default value if required
	target_system=$(option_value 'package')
	case "$target_system" in
		('arch')
			local package package_architecture
			package=$(current_package)
			package_architecture=$(package_architecture "$package")
			if [ "$package_architecture" = '32' ]; then
				path_structure='%s/lib32/games/%s'
			fi
		;;
		('gentoo'|'egentoo')
			local package package_architecture
			package=$(current_package)
			package_architecture=$(package_architecture "$package")
			if [ "$package_architecture" = '64' ]; then
				path_structure='%s/lib64/games/%s'
			fi
		;;
	esac

	## Silence ShellCheck false-positive
	## Don't use variables in the printf format string. Use printf "..%s.." "$foo".
	# shellcheck disable=SC2059
	printf "$path_structure" "$install_prefix" "$game_id"
}

# Print the path to system libraries
# USAGE: path_libraries_system
path_libraries_system() {
	local path_libraries current_package package_architecture option_package
	current_package=$(current_package)
	package_architecture=$(package_architecture "$current_package")
	option_package=$(option_value 'package')
	case "$option_package" in
		('arch')
			case "$package_architecture" in
				('32')
					path_libraries='/usr/lib32'
				;;
				('64')
					path_libraries='/usr/lib'
				;;
			esac
		;;
		('deb')
			case "$package_architecture" in
				('32')
					path_libraries='/usr/lib/i386-linux-gnu'
				;;
				('64')
					path_libraries='/usr/lib/x86_64-linux-gnu'
				;;
			esac
		;;
		('gentoo'|'egentoo')
			case "$package_architecture" in
				('32')
					path_libraries='/usr/lib'
				;;
				('64')
					path_libraries='/usr/lib64'
				;;
			esac
		;;
	esac

	if [ -z "${path_libraries:-}" ]; then
		error_path_libraries_system_unsupported_architecture "$current_package"
		return 1
	fi

	printf '%s' "$path_libraries"
}

# Print install path for TTF fonts.
# USAGE: path_fonts_ttf
path_fonts_ttf() {
	local game_id
	game_id=$(game_id)

	# Fonts are always installed under the default install prefix.
	printf '/usr/share/fonts/truetype/%s' "$game_id"
}

