# Build the library

.PHONY: all clean

all: lib/libplayit2.sh

lib/libplayit2.sh: src/*/*.sh
	mkdir --parents lib
	find src -type f -name '*.sh' -print0 | LC_ALL=C sort -z | xargs -0 cat > lib/libplayit2.sh

clean:
	rm --force lib/libplayit2.sh


# Install the library, main script, and man pages

.PHONY: install uninstall

## Set the default install paths
UID := $(shell id --user)
ifeq ($(UID),0)
    prefix = /usr/local
    bindir = $(prefix)/games
    datadir = $(prefix)/share/games
    mandir = $(prefix)/share/man
else
    ifeq ($(XDG_DATA_HOME),)
        XDG_DATA_HOME := $(HOME)/.local/share
    endif
    prefix = $(XDG_DATA_HOME)
    bindir = $(HOME)/.local/bin
    datadir = $(prefix)
    mandir = $(prefix)/man
endif

install: all
	install -D --mode=644 lib/libplayit2.sh $(DESTDIR)$(datadir)/play.it/libplayit2.sh
	install -D --mode=755 play.it $(DESTDIR)$(bindir)/play.it
	install -D --mode=644 man/man6/play.it.6 $(DESTDIR)$(mandir)/man6/play.it.6
	install -D --mode=644 man/fr/man6/play.it.6 $(DESTDIR)$(mandir)/fr/man6/play.it.6

uninstall:
	rm --force $(DESTDIR)$(bindir)/play.it $(DESTDIR)$(datadir)/play.it/libplayit2.sh $(DESTDIR)$(mandir)/man6/play.it.6 $(DESTDIR)$(mandir)/fr/man6/play.it.6
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(bindir) $(DESTDIR)$(datadir)/play.it $(DESTDIR)$(mandir)/man6 $(DESTDIR)$(mandir)/fr/man6


# Release preparation

.PHONY: dist

## The generated tarball is signed with gpg by default,
## NO_SIGN should be set to a non-0 value to skip the signature.
NO_SIGN := 0

dist: VERSION = $(shell head --lines=1 CHANGELOG)
dist: TARBALL = play.it-$(VERSION).tar.gz
dist: TAR_OPTIONS := --sort=name --mtime=2017-06-14 --owner=root:0 --group=root:0 --use-compress-program='gzip --no-name'
dist: CHANGELOG LICENSE README.md Makefile play.it man/man6/play.it.6 man/*/man6/play.it.6 src/*/*.sh tests/shunit2/*/*.sh
	mkdir --parents dist
	LC_ALL=C tar cf dist/$(TARBALL) $(TAR_OPTIONS) CHANGELOG LICENSE README.md Makefile play.it man/man6/play.it.6 man/*/man6/play.it.6 src/*/*.sh tests/shunit2/*/*.sh
ifeq ($(NO_SIGN),0)
	rm --force dist/$(TARBALL).asc
	gpg --armor --detach-sign dist/$(TARBALL)
endif


# Run tests, including:
# - syntax checks, relying on ShellCheck
# - unit tests, relying on shUnit2
# - man page syntax check

.PHONY: check shellcheck-library shellcheck-wrapper shunit2 man-syntax

check: shellcheck-library shellcheck-wrapper shunit2 man-syntax

## This is a unicode quote. Delete and retype it (or ignore/doublequote for literal).
shellcheck-library: SHELLCHECK_EXCLUDE += --exclude=SC1112
## Expressions don't expand in single quotes, use double quotes for that.
shellcheck-library: SHELLCHECK_EXCLUDE += --exclude=SC2016
## Double quote to prevent globbing and word splitting.
shellcheck-library: SHELLCHECK_EXCLUDE += --exclude=SC2086
## In POSIX sh, local is undefined.
shellcheck-library: SHELLCHECK_EXCLUDE += --exclude=SC3043
shellcheck-library:
	shellcheck --shell=sh $(SHELLCHECK_EXCLUDE) lib/libplayit2.sh

shellcheck-wrapper:
	shellcheck --external-sources --shell=sh play.it

SHUNIT2_SCRIPTS := $(wildcard tests/shunit2/*/*.sh)
SHUNIT2_TESTS := $(addprefix shunit2_, $(SHUNIT2_SCRIPTS))
.PHONY: $(SHUNIT2_TESTS)
shunit2: $(SHUNIT2_TESTS)
$(SHUNIT2_TESTS): shunit2_%: %
	shunit2 $<

man-syntax:
	man --warnings --encoding=UTF-8 --local-file --troff-device=utf8 --ditroff man/man6/play.it.6 >/dev/null
	man --warnings --encoding=UTF-8 --local-file --troff-device=utf8 --ditroff man/fr/man6/play.it.6 >/dev/null
