#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

setUp() {
	# Set a temporary directory to mess with real files
	TEST_TEMP_DIR=$(mktemp --directory)
	export TEST_TEMP_DIR
}

tearDown() {
	rm --force --recursive "$TEST_TEMP_DIR"
}

test_unity3d_application_exe_default() {
	# Check that the correct Unity3D game binary is picked up
	local CONTENT_PATH_DEFAULT PLAYIT_WORKDIR UNITY3D_NAME
	CONTENT_PATH_DEFAULT='.'
	PLAYIT_WORKDIR="$TEST_TEMP_DIR"
	UNITY3D_NAME='Dungeons2'
	mkdir --parents "${PLAYIT_WORKDIR}/gamedata/${CONTENT_PATH_DEFAULT}"
	touch \
		"${PLAYIT_WORKDIR}/gamedata/${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}" \
		"${PLAYIT_WORKDIR}/gamedata/${CONTENT_PATH_DEFAULT}/${UNITY3D_NAME}.x86"
	assertEquals \
		'unity3d_application_exe_default picked up the wrong file as the main game binary.' \
		"${UNITY3D_NAME}.x86" \
		"$(unity3d_application_exe_default 'APP_MAIN')"

	return 0
}
