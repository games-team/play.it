#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_application_type() {
	local APP_MAIN_TYPE APP_MAIN_SCUMMID application_type

	APP_MAIN_SCUMMID='engine:game'
	application_type=$(application_type 'APP_MAIN')
	assertEquals 'scummvm' "$application_type"

	APP_MAIN_TYPE='native'
	application_type=$(application_type 'APP_MAIN')
	assertEquals 'native' "$application_type"

	# Test that invalid types are rejected
	APP_MAIN_TYPE='invalid'
	assertFalse "application_type 'APP_MAIN'"
}
