#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_native_launcher() {
	local APP_MAIN_PREFIX_TYPE

	APP_MAIN_PREFIX_TYPE='symlinks'
	assertTrue \
		'Failed to generate a launcher for a native Linux game using a symlinks prefix.' \
		'native_launcher "APP_MAIN"'

	APP_MAIN_PREFIX_TYPE='none'
	assertTrue \
		'Failed to generate a launcher for a native Linux game not using a prefix.' \
		'native_launcher "APP_MAIN"'
}
