#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_hack_path_source() {
	local hack_path_source PLAYIT_WORKDIR HACK_SDL1COMPAT_NAME

	PLAYIT_WORKDIR='/some/path'
	HACK_SDL1COMPAT_NAME='smacshim'
	hack_path_source=$(hack_path_source 'HACK_SDL1COMPAT')
	assertEquals "${PLAYIT_WORKDIR}/hacks/smacshim.c" "$hack_path_source"
}

test_hack_path_library() {
	local hack_path_library PLAYIT_WORKDIR HACK_SDL1COMPAT_NAME

	PLAYIT_WORKDIR='/some/path'
	HACK_SDL1COMPAT_NAME='smacshim'
	hack_path_library=$(hack_path_library 'HACK_SDL1COMPAT')
	assertEquals "${PLAYIT_WORKDIR}/hacks/smacshim.so" "$hack_path_library"
}
