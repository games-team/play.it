#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_hacks_list() {
	local \
		hacks_list hacks_list_expected \
		PRELOAD_HACKS_LIST PRELOAD_HACKS_LIST_MAIN \
		PLAYIT_CONTEXT_ARCHIVE

	hacks_list=$(hacks_list)
	assertNull "$hacks_list"

	PRELOAD_HACKS_LIST='
HACK_DEFAULT1
HACK_DEFAULT2'
	hacks_list=$(hacks_list)
	hacks_list_expected='HACK_DEFAULT1
HACK_DEFAULT2'
	assertEquals "$hacks_list_expected" "$hacks_list"

	set_current_archive 'ARCHIVE_BASE_MAIN_0'
	PRELOAD_HACKS_LIST_MAIN='
HACK_OTHER1
HACK_OTHER2'
	hacks_list=$(hacks_list)
	hacks_list_expected='HACK_OTHER1
HACK_OTHER2'
	assertEquals "$hacks_list_expected" "$hacks_list"
}
