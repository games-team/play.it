#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_hack_name() {
	local hack_name HACK_SDL1COMPAT_NAME

	HACK_SDL1COMPAT_NAME='smacshim'
	hack_name=$(hack_name 'HACK_SDL1COMPAT')
	assertEquals 'smacshim' "$hack_name"

	# Values spanning multiple lines are not allowed
	HACK_SDL1COMPAT_NAME='smac
shim'
	assertFalse 'hack_name "HACK_SDL1COMPAT"'

	# Empty values are not allowed
	HACK_SDL1COMPAT_NAME=''
	assertFalse 'hack_name "HACK_SDL1COMPAT"'
}

test_hack_description() {
	local hack_description HACK_SDL1COMPAT_DESCRIPTION

	HACK_SDL1COMPAT_DESCRIPTION='Build and include the LD_PRELOAD shim allowing the old engine to run on top of latest SDL1 library'
	hack_description=$(hack_description 'HACK_SDL1COMPAT')
	assertEquals 'Build and include the LD_PRELOAD shim allowing the old engine to run on top of latest SDL1 library' "$hack_description"

	# Empty values are not allowed
	HACK_SDL1COMPAT_DESCRIPTION=''
	assertFalse 'hack_description "HACK_SDL1COMPAT"'
}

test_hack_package() {
	local hack_package HACK_SDL1COMPAT_PACKAGE PLAYIT_CONTEXT_PACKAGE

	HACK_SDL1COMPAT_PACKAGE='PKG_BIN'
	hack_package=$(hack_package 'HACK_SDL1COMPAT')
	assertEquals 'PKG_BIN' "$hack_package"

	# If no value is set, fall back on the current package
	unset HACK_SDL1COMPAT_PACKAGE
	set_current_package 'PKG_MAIN'
	hack_package=$(hack_package 'HACK_SDL1COMPAT')
	assertEquals 'PKG_MAIN' "$hack_package"
}

test_hack_source() {
	local hack_source hack_source_expected HACK_SDL1COMPAT_SOURCE

	HACK_SDL1COMPAT_SOURCE='
#define something

#include <something else>

void __do_something(some args) {
	# Nothing to see here
}
'
	hack_source=$(hack_source 'HACK_SDL1COMPAT')
	## Extra line breaks at the end of the source file content are dropped during the variable assignation.
	hack_source_expected='
#define something

#include <something else>

void __do_something(some args) {
	# Nothing to see here
}'
	assertEquals "$hack_source_expected" "$hack_source"

	# Empty values are not allowed
	HACK_SDL1COMPAT_SOURCE=''
	assertFalse 'hack_source "HACK_SDL1COMPAT"'
}
