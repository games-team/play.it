#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_hack_application_prerun() {
	local hack_application_prerun hack_application_prerun_expected HACK_SDL1COMPAT_NAME HACK_SDL1COMPAT_DESCRIPTION

	path_libraries() {
		printf '/usr/lib/games/some-game'
	}

	HACK_SDL1COMPAT_NAME='smacshim'
	HACK_SDL1COMPAT_DESCRIPTION='LD_PRELOAD shim allowing the old engine to run on top of latest SDL1 library
cf. https://github.com/ZeroPointEnergy/smacshim'
	hack_application_prerun=$(hack_application_prerun 'HACK_SDL1COMPAT')
	hack_application_prerun_expected='# LD_PRELOAD shim allowing the old engine to run on top of latest SDL1 library
# cf. https://github.com/ZeroPointEnergy/smacshim
export LD_PRELOAD="${LD_PRELOAD}:/usr/lib/games/some-game/preload-hacks/smacshim.so"'
	assertEquals "$hack_application_prerun_expected" "$hack_application_prerun"
}

test_hacks_included_in_package() {
	local \
		hacks_list hacks_list_expected \
		PRELOAD_HACKS_LIST HACK_INCLUDED1_PACKAGE HACK_INCLUDED2_PACKAGE HACK_EXCLUDED_PACKAGE

	PRELOAD_HACKS_LIST='
HACK_INCLUDED1
HACK_INCLUDED2
HACK_EXCLUDED'
	HACK_INCLUDED1_PACKAGE='PKG_BIN'
	HACK_INCLUDED2_PACKAGE='PKG_BIN'
	HACK_EXCLUDED_PACKAGE='PKG_MAIN'
	hacks_list=$(hacks_included_in_package 'PKG_BIN')
	hacks_list_expected='HACK_INCLUDED1
HACK_INCLUDED2'
	assertEquals "$hacks_list_expected" "$hacks_list"
}
