#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_icons_list_all() {
	local APPLICATIONS_LIST APP_MAIN_ICONS_LIST APP_EXTRA_ICONS_LIST icons_list icons_list_expected

	APPLICATIONS_LIST='APP_MAIN APP_EXTRA'
	APP_MAIN_ICONS_LIST='APP_MAIN_ICON'
	APP_EXTRA_ICONS_LIST='APP_EXTRA_ICON1 APP_EXTRA_ICON2'
	icons_list=$(icons_list_all)
	icons_list_expected='APP_EXTRA_ICON1
APP_EXTRA_ICON2
APP_MAIN_ICON'
	assertEquals "$icons_list_expected" "$icons_list"
}

test_application_icons_list() {
	local APP_MAIN_ICONS_LIST APP_MAIN_ICON APP_MAIN_TYPE GAME_ENGINE icons_list

	APP_MAIN_ICONS_LIST='APP_MAIN_ICON1 APP_MAIN_ICON_2'
	icons_list=$(application_icons_list 'APP_MAIN')
	assertEquals 'APP_MAIN_ICON1 APP_MAIN_ICON_2' "$icons_list"
	unset APP_MAIN_ICONS_LIST

	APP_MAIN_ICON='something.ico'
	icons_list=$(application_icons_list 'APP_MAIN')
	assertEquals 'APP_MAIN_ICON' "$icons_list"
	unset APP_MAIN_ICON

	APP_MAIN_TYPE='wine'
	icons_list=$(application_icons_list 'APP_MAIN')
	assertEquals 'APP_MAIN_ICON' "$icons_list"
	unset APP_MAIN_TYPE

	GAME_ENGINE='unity3d'
	icons_list=$(application_icons_list 'APP_MAIN')
	assertEquals 'APP_MAIN_ICON' "$icons_list"
	unset GAME_ENGINE

	icons_list=$(application_icons_list 'APP_MAIN')
	assertNull "$icons_list"
}

test_icon_application() {
	local APPLICATIONS_LIST application

	APPLICATIONS_LIST='APP_MAIN APP_EXTRA'
	application=$(icon_application 'APP_MAIN_ICON')
	assertEquals 'APP_MAIN' "$application"
	application=$(icon_application 'APP_EXTRA_ICON1')
	assertEquals 'APP_EXTRA' "$application"

	# Check that icon_application does not get confused between applications with similar names.
	APPLICATIONS_LIST='APP_HOF APP_HOFEDIT'
	APP_HOF_ID='heroes-of-might-and-magic-5-hammers-of-fate'
	APP_HOFEDIT_ID='heroes-of-might-and-magic-5-hammers-of-fate-map-editor'
	application=$(icon_application 'APP_HOFEDIT_ID')
	assertEquals 'icon_application linked an icon to the wrong application.' 'APP_HOFEDIT' "$application"
}

test_icon_path() {
	local \
		APP_MAIN_ICON icon_path \
		APPLICATIONS_LIST APP_MAIN_TYPE APP_MAIN_EXE \
		GAME_ENGINE UNITY3D_NAME

	APP_MAIN_ICON='something.ico'
	icon_path=$(icon_path 'APP_MAIN_ICON')
	assertEquals 'something.ico' "$APP_MAIN_ICON"
	unset APP_MAIN_ICON

	APPLICATIONS_LIST='APP_MAIN'
	APP_MAIN_TYPE='wine'
	APP_MAIN_EXE='something.exe'
	icon_path=$(icon_path 'APP_MAIN_ICON')
	assertEquals 'something.exe' "$icon_path"
	unset APPLICATIONS_LIST APP_MAIN_TYPE APP_MAIN_EXE

	GAME_ENGINE='unity3d'
	UNITY3D_NAME='Something'
	APP_MAIN_TYPE='native'
	icon_path=$(icon_path 'APP_MAIN_ICON')
	assertEquals 'Something_Data/Resources/UnityPlayer.png' "$icon_path"
	APP_MAIN_TYPE='wine'
	icon_path=$(icon_path 'APP_MAIN_ICON')
	assertEquals 'Something.exe' "$icon_path"
	unset GAME_ENGINE UNITY3D_NAME APP_MAIN_TYPE
}

test_icon_wrestool_options() {
	local \
		APP_MAIN_ICON APP_MAIN_ICON_WRESTOOL_OPTIONS wrestool_options \
		APPLICATIONS_LIST GAME_ENGINE \
		APP_MAIN_ICON_ID

	# Check that an error is triggered if the icon is not using the expected file extension
	APP_MAIN_ICON='something.ico'
	assertFalse "wrestool_options 'APP_MAIN_ICON'"

	APP_MAIN_ICON='something.exe'
	APP_MAIN_ICON_WRESTOOL_OPTIONS='--type=14 --name=221'
	wrestool_options=$(icon_wrestool_options 'APP_MAIN_ICON')
	assertEquals '--type=14 --name=221' "$wrestool_options"
	unset APP_MAIN_ICON_WRESTOOL_OPTIONS

	APPLICATIONS_LIST='APP_MAIN'
	GAME_ENGINE='unrealengine4'
	wrestool_options=$(icon_wrestool_options 'APP_MAIN_ICON')
	assertEquals '--type=14 --name=101' "$wrestool_options"
	unset GAME_ENGINE

	wrestool_options=$(icon_wrestool_options 'APP_MAIN_ICON')
	assertEquals '--type=14' "$wrestool_options"
}
