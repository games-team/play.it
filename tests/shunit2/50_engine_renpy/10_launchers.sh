#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_renpy_launcher() {
	local APP_MAIN_PREFIX_TYPE

	APP_MAIN_PREFIX_TYPE='symlinks'
	assertTrue \
		"Failed to generate a launcher for a Ren'Py game using a symlinks prefix." \
		'renpy_launcher "APP_MAIN"'

	APP_MAIN_PREFIX_TYPE='none'
	assertTrue \
		"Failed to generate a launcher for a Ren'Py game not using a prefix." \
		'renpy_launcher "APP_MAIN"'
}
