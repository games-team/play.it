#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_gentoo_field_description() {
	local field_description script_version GAME_NAME PKG_MAIN_DESCRIPTION

	script_version='19700101.1'
	GAME_NAME='Alpha Centauri'
	PKG_MAIN_DESCRIPTION='French localization'
	field_description=$(gentoo_field_description 'PKG_MAIN')
	assertEquals 'Alpha Centauri - French localization - ./play.it script version 19700101.1' "$field_description"
	unset PKG_MAIN_DESCRIPTION

	field_description=$(gentoo_field_description 'PKG_MAIN')
	assertEquals 'Alpha Centauri - ./play.it script version 19700101.1' "$field_description"
}
