#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_archlinux_field_pkgdesc() {
	local field_pkgdesc script_version GAME_NAME PKG_MAIN_DESCRIPTION

	script_version='19700101.1'
	GAME_NAME='Alpha Centauri'
	PKG_MAIN_DESCRIPTION='French localization'
	field_pkgdesc=$(archlinux_field_pkgdesc 'PKG_MAIN')
	assertEquals 'Alpha Centauri - French localization - ./play.it script version 19700101.1' "$field_pkgdesc"
	unset PKG_MAIN_DESCRIPTION

	field_pkgdesc=$(archlinux_field_pkgdesc 'PKG_MAIN')
	assertEquals 'Alpha Centauri - ./play.it script version 19700101.1' "$field_pkgdesc"
}
