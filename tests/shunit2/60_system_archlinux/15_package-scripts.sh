#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_archlinux_script_install() {
	local \
		script_install script_install_expected \
		PKG_MAIN_POSTINST_RUN PKG_MAIN_PRERM_RUN \
		PKG_MAIN_POSTINST_WARNINGS

	PKG_MAIN_POSTINST_RUN="# Link common files shared by the games series
ln --symbolic '/usr/share/games/heroes-chronicles/data' '/usr/share/games/heroes-chronicles-3'
ln --symbolic '/usr/share/games/heroes-chronicles/mp3' '/usr/share/games/heroes-chronicles-3'"
	PKG_MAIN_PRERM_RUN="# Delete links to common files shared by the games series
rm '/usr/share/games/heroes-chronicles-3/mp3'
rm '/usr/share/games/heroes-chronicles-3/data'"
	script_install=$(archlinux_script_install 'PKG_MAIN')
	script_install_expected="post_install() {
# Link common files shared by the games series
ln --symbolic '/usr/share/games/heroes-chronicles/data' '/usr/share/games/heroes-chronicles-3'
ln --symbolic '/usr/share/games/heroes-chronicles/mp3' '/usr/share/games/heroes-chronicles-3'
}

post_upgrade() {
post_install
}
pre_remove() {
# Delete links to common files shared by the games series
rm '/usr/share/games/heroes-chronicles-3/mp3'
rm '/usr/share/games/heroes-chronicles-3/data'
}

pre_upgrade() {
pre_remove
}"
	assertEquals "$script_install_expected" "$script_install"
	unset PKG_MAIN_POSTINST_RUN PKG_MAIN_PRERM_RUN

	PKG_MAIN_POSTINST_WARNINGS='You may need to generate the ja_JP.UTF-8 locale for the configuration program to run
You need a MIDI synthetiser for music to play in the game (you can use timidity++ or fluidsynth if you don’t have a hardware synthetiser)'
	script_install=$(archlinux_script_install 'PKG_MAIN')
	script_install_expected='post_install() {
printf "Warning: %s\n" "You may need to generate the ja_JP.UTF-8 locale for the configuration program to run"
printf "Warning: %s\n" "You need a MIDI synthetiser for music to play in the game (you can use timidity++ or fluidsynth if you don’t have a hardware synthetiser)"
}

post_upgrade() {
post_install
}'
	assertEquals "$script_install_expected" "$script_install"

}
