#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_persistent_list_directories() {
	local \
		persistent_directories persistent_directories_expected \
		USER_PERSISTENT_DIRECTORIES \
		CONFIG_DIRS DATA_DIRS PLAYIT_COMPATIBILITY_LEVEL

	USER_PERSISTENT_DIRECTORIES='
mods
users'
	persistent_directories=$(persistent_list_directories)
	persistent_directories_expected='mods
users'
	assertEquals "$persistent_directories_expected" "$persistent_directories"
	unset USER_PERSISTENT_DIRECTORIES

	# Check that empty values do not trigger an error
	assertTrue 'persistent_list_directories'
}

test_persistent_list_files() {
	local \
		persistent_files persistent_files_expected \
		USER_PERSISTENT_FILES \
		CONFIG_FILES DATA_FILES PLAYIT_COMPATIBILITY_LEVEL

	USER_PERSISTENT_FILES='
*.cfg
*.dat
player-data.json'
	persistent_files=$(persistent_list_files)
	persistent_files_expected='*.cfg
*.dat
player-data.json'
	assertEquals "$persistent_files_expected" "$persistent_files"
	unset USER_PERSISTENT_FILES

	# Check that empty values do not trigger an error
	assertTrue 'persistent_list_files'
}
