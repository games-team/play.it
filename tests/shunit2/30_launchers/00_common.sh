#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_launcher_generation_checks() {
	local APP_MAIN_TYPE APP_MAIN_EXE APP_MAIN_SCUMMID

	# Disable launcher_target_presence_check,
	# it should be tested in its own function.
	launcher_target_presence_check() { return 0; }

	# Ensure that an error is triggered on missing application type.
	unset APP_MAIN_TYPE
	assertFalse 'launcher_generation_checks "PKG_MAIN" "APP_MAIN"'

	# Ensure that an error is triggered when no binary path is set,
	# but one is expected by the current application type.
	unset APP_MAIN_EXE
	for APP_MAIN_TYPE in \
		'dosbox' \
		'java' \
		'mono' \
		'native' \
		'wine'
	do
		assertFalse 'launcher_generation_checks "PKG_MAIN" "APP_MAIN"'
	done

	# Ensure that an error is triggered when no ScummVM game ID is set,
	# but one is expected by the current application type.
	APP_MAIN_TYPE='scummvm'
	assertFalse 'launcher_generation_checks "PKG_MAIN" "APP_MAIN"'
}
