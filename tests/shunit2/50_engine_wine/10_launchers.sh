#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_wine_launcher() {
	local APP_MAIN_PREFIX_TYPE

	APP_MAIN_PREFIX_TYPE='symlinks'
	assertTrue \
		'Failed to generate a launcher for a WINE game using a symlinks prefix.' \
		'wine_launcher "APP_MAIN"'
}
