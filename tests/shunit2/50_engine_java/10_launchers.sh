#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_java_launcher() {
	local APP_MAIN_PREFIX_TYPE

	APP_MAIN_PREFIX_TYPE='symlinks'
	assertTrue \
		'Failed to generate a launcher for a Java game using a symlinks prefix.' \
		'java_launcher "APP_MAIN"'
}
