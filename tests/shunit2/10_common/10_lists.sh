#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_list_clean() {
	local list_unsorted list_sorted list_expected
	list_unsorted='aaa
	ccc

	bbb
	aaa   '
	list_expected='aaa
bbb
ccc'
	list_sorted=$(printf '%s' "$list_unsorted" | list_clean)
	assertEquals "$list_expected" "$list_sorted"

}
