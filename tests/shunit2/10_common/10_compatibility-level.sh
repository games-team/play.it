#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_compatibility_level() {
	local compatibility_level PLAYIT_COMPATIBILITY_LEVEL target_version LIBRARY_VERSION

	PLAYIT_COMPATIBILITY_LEVEL='2.22'
	compatibility_level=$(compatibility_level)
	assertEquals '2.22' "$compatibility_level"
	unset PLAYIT_COMPATIBILITY_LEVEL

	target_version='2.21'
	compatibility_level=$(compatibility_level)
	assertEquals '2.21' "$compatibility_level"
	unset target_version

	## The LIBRARY_VERSION value is used as a fallback if no compatibility level is set from the game script.
	LIBRARY_VERSION='1.42.0'
	compatibility_level=$(compatibility_level)
	assertEquals '1.42' "$compatibility_level"
	unset LIBRARY_VERSION

	## Check that errors are thrown on invalid values.
	PLAYIT_COMPATIBILITY_LEVEL='2'
	assertFalse 'compatibility_level'
	PLAYIT_COMPATIBILITY_LEVEL='2.29.0'
	assertFalse 'compatibility_level'
	unset PLAYIT_COMPATIBILITY_LEVEL
}

test_compatibility_level_is_at_least() {
	local PLAYIT_COMPATIBILITY_LEVEL

	PLAYIT_COMPATIBILITY_LEVEL='2.21'
	assertTrue 'compatibility_level_is_at_least "1.0"'
	assertTrue 'compatibility_level_is_at_least "2.0"'
	assertTrue 'compatibility_level_is_at_least "2.21"'
	assertFalse 'compatibility_level_is_at_least "2.42"'
	assertFalse 'compatibility_level_is_at_least "3.0"'
}
