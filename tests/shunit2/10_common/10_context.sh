#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_set_current_archive() {
	local PLAYIT_CONTEXT_ARCHIVE

	set_current_archive 'ARCHIVE_BASE_0'
	assertEquals 'ARCHIVE_BASE_0' "$PLAYIT_CONTEXT_ARCHIVE"

	# Check that the format ARCHIVE_BASE_xxx is enforced
	assertFalse 'set_current_archive "ARCHIVE_GOG_OLD0"'
}

test_set_current_package() {
	local PLAYIT_CONTEXT_PACKAGE PACKAGES_LIST

	set_current_package 'PKG_MAIN'
	assertEquals 'PKG_MAIN' "$PLAYIT_CONTEXT_PACKAGE"

	# Check that the format PKG_xxx is enforced
	assertFalse 'set_current_package "PACKAGE_MAIN"'

	# An error should be thrown when trying to set a package that is not included in the list of packages to build.
	PACKAGES_LIST='
	PKG_BIN
	PKG_DATA'
	assertFalse 'set_current_package "PKG_BIN32"'
}

test_set_default_package() {
	local PLAYIT_CONTEXT_PACKAGE_DEFAULT PACKAGES_LIST

	set_default_package 'PKG_MAIN'
	assertEquals 'PKG_MAIN' "$PLAYIT_CONTEXT_PACKAGE_DEFAULT"

	# Check that the format PKG_xxx is enforced
	assertFalse 'set_default_package "PACKAGE_MAIN"'

	# An error should be thrown when trying to set a package that is not included in the list of packages to build.
	PACKAGES_LIST='
	PKG_BIN
	PKG_DATA'
	assertFalse 'set_default_package "PKG_BIN32"'
}

test_current_archive() {
	local current_archive PLAYIT_COMPATIBILITY_LEVEL PLAYIT_CONTEXT_ARCHIVE ARCHIVE

	current_archive=$(current_archive)
	assertNull "$current_archive"

	set_current_archive 'ARCHIVE_BASE_0'
	current_archive=$(current_archive)
	assertEquals 'ARCHIVE_BASE_0' "$current_archive"
	unset PLAYIT_CONTEXT_ARCHIVE

	# Set the current archive using the legacy global variable
	PLAYIT_COMPATIBILITY_LEVEL='2.25'
	ARCHIVE='ARCHIVE_BASE_1'
	current_archive=$(current_archive)
	assertEquals 'ARCHIVE_BASE_1' "$current_archive"
	unset PLAYIT_COMPATIBILITY_LEVEL PLAYIT_CONTEXT_ARCHIVE ARCHIVE

	# Check the priority order between the current context system and the legacy one.
	PLAYIT_COMPATIBILITY_LEVEL='2.25'
	set_current_archive 'ARCHIVE_BASE_2'
	ARCHIVE='ARCHIVE_BASE_3'
	current_archive=$(current_archive)
	assertEquals \
		'current_archive ignored the value of $ARCHIVE because set_current_archive has been used. This will break compatibility with game scripts relying on the ability to set the context using $ARCHIVE.' \
		'ARCHIVE_BASE_3' "$current_archive"
	unset PLAYIT_COMPATIBILITY_LEVEL PLAYIT_CONTEXT_ARCHIVE ARCHIVE
}

test_current_package() {
	local PACKAGES_LIST PLAYIT_CONTEXT_PACKAGE_DEFAULT PLAYIT_CONTEXT_PACKAGE PLAYIT_COMPATIBILITY_LEVEL PKG

	assertNull "$(current_package)"

	PACKAGES_LIST='
	PKG_BIN32
	PKG_BIN64
	PKG_L10N
	PKG_DATA'
	set_default_package 'PKG_BIN32'
	assertEquals 'PKG_BIN32' "$(current_package)"

	set_current_package 'PKG_BIN64'
	assertEquals 'PKG_BIN64' "$(current_package)"

	# Backwards compatibility with compatibility level ≤ 2.25
	PLAYIT_COMPATIBILITY_LEVEL='2.25'
	## Set the current package using the legacy global variable
	PKG='PKG_L10N'
	assertEquals 'PKG_L10N' "$(current_package)"
	## Check the priority order between the current context system and the legacy one
	PKG='PKG_DATA'
	current_package=$(current_package)
	assertEquals \
		'current_package ignored the value of $PKG because set_current_package has been used. This will break compatibility with game scripts relying on the ability to set the context using $PKG.' \
		'PKG_DATA' "$(current_package)"
}

test_default_package() {
	local PLAYIT_CONTEXT_PACKAGE_DEFAULT

	assertNull "$(default_package)"

	set_default_package 'PKG_MAIN'
	assertEquals 'PKG_MAIN' "$(default_package)"
}

test_current_archive_suffix() {
	local PLAYIT_CONTEXT_ARCHIVE

	set_current_archive 'ARCHIVE_BASE_MULTIARCH_0'
	assertEquals '_MULTIARCH_0' "$(current_archive_suffix)"
}

test_current_package_suffix() {
	local current_package_suffix PLAYIT_CONTEXT_PACKAGE PACKAGES_LIST

	PACKAGES_LIST='
	PKG_BIN32
	PKG_BIN64
	PKG_DATA'
	set_current_package 'PKG_BIN32'
	current_package_suffix=$(current_package_suffix)
	assertEquals '_BIN32' "$current_package_suffix"
}

test_context_name() {
	local \
		context_name \
		PLAYIT_CONTEXT_ARCHIVE PLAYIT_CONTEXT_PACKAGE PACKAGES_LIST \
		SOME_VARIABLE SOME_VARIABLE_BIN32 SOME_VARIABLE_MULTIARCH SOME_VARIABLE_MULTIARCH_0

	set_current_archive 'ARCHIVE_BASE_MULTIARCH_0'
	PACKAGES_LIST='
	PKG_BIN32
	PKG_BIN64
	PKG_DATA'
	set_current_package 'PKG_BIN32'

	context_name=$(context_name 'SOME_VARIABLE')
	assertNull "$context_name"

	SOME_VARIABLE='some value'
	context_name=$(context_name 'SOME_VARIABLE')
	assertEquals 'SOME_VARIABLE' "$context_name"

	SOME_VARIABLE_BIN32='some value'
	context_name=$(context_name 'SOME_VARIABLE')
	assertEquals 'SOME_VARIABLE_BIN32' "$context_name"

	SOME_VARIABLE_MULTIARCH='some value'
	context_name=$(context_name 'SOME_VARIABLE')
	assertEquals 'SOME_VARIABLE_MULTIARCH' "$context_name"

	SOME_VARIABLE_MULTIARCH_0='some value'
	context_name=$(context_name 'SOME_VARIABLE')
	assertEquals 'SOME_VARIABLE_MULTIARCH_0' "$context_name"
}

test_context_name_archive() {
	local \
		context_name_archive \
		PLAYIT_CONTEXT_ARCHIVE \
		SOME_VARIABLE SOME_VARIABLE_MULTIARCH SOME_VARIABLE_MULTIARCH_0

	set_current_archive 'ARCHIVE_BASE_MULTIARCH_0'

	context_name_archive=$(context_name_archive 'SOME_VARIABLE')
	assertNull "$context_name_archive"

	SOME_VARIABLE='some value'
	context_name_archive=$(context_name_archive 'SOME_VARIABLE')
	assertNull "$context_name_archive"

	SOME_VARIABLE_MULTIARCH='some value'
	context_name_archive=$(context_name_archive 'SOME_VARIABLE')
	assertEquals 'SOME_VARIABLE_MULTIARCH' "$context_name_archive"

	SOME_VARIABLE_MULTIARCH_0='some value'
	context_name_archive=$(context_name_archive 'SOME_VARIABLE')
	assertEquals 'SOME_VARIABLE_MULTIARCH_0' "$context_name_archive"
}

test_context_name_package() {
	local \
		context_name_package \
		PLAYIT_CONTEXT_PACKAGE PACKAGES_LIST \
		SOME_VARIABLE SOME_VARIABLE_BIN32

	PACKAGES_LIST='
	PKG_BIN32
	PKG_BIN64
	PKG_DATA'
	set_current_package 'PKG_BIN32'

	context_name_package=$(context_name_package 'SOME_VARIABLE')
	assertNull "$context_name_package"

	SOME_VARIABLE='some value'
	context_name_package=$(context_name_package 'SOME_VARIABLE')
	assertNull "$context_name_package"

	SOME_VARIABLE_BIN32='some value'
	context_name_package=$(context_name_package 'SOME_VARIABLE')
	assertEquals 'SOME_VARIABLE_BIN32' "$context_name_package"
}

test_context_value() {
	local \
		context_value \
		PLAYIT_CONTEXT_ARCHIVE PLAYIT_CONTEXT_PACKAGE PACKAGES_LIST \
		SOME_VARIABLE SOME_VARIABLE_BIN32 SOME_VARIABLE_MULTIARCH SOME_VARIABLE_MULTIARCH_0

	set_current_archive 'ARCHIVE_BASE_MULTIARCH_0'
	PACKAGES_LIST='
	PKG_BIN32
	PKG_BIN64
	PKG_DATA'
	set_current_package 'PKG_BIN32'

	context_value=$(context_value 'SOME_VARIABLE')
	assertNull "$context_value"

	SOME_VARIABLE='some value'
	context_value=$(context_value 'SOME_VARIABLE')
	assertEquals 'some value' "$context_value"

	SOME_VARIABLE_BIN32='some value specific to PKG_BIN32'
	context_value=$(context_value 'SOME_VARIABLE')
	assertEquals 'some value specific to PKG_BIN32' "$context_value"

	SOME_VARIABLE_MULTIARCH='some value specific to ARCHIVE_BASE_MULTIARCH_*'
	context_value=$(context_value 'SOME_VARIABLE')
	assertEquals 'some value specific to ARCHIVE_BASE_MULTIARCH_*' "$context_value"

	SOME_VARIABLE_MULTIARCH_0='some value specific to ARCHIVE_BASE_MULTIARCH_0'
	context_value=$(context_value 'SOME_VARIABLE')
	assertEquals 'some value specific to ARCHIVE_BASE_MULTIARCH_0' "$context_value"
}
