#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_game_engine() {
	local game_engine GAME_ENGINE UNITY3D_NAME UNREALENGINE4_NAME

	GAME_ENGINE='visionaire'
	game_engine=$(game_engine)
	assertEquals 'visionaire' "$game_engine"
	unset GAME_ENGINE

	UNITY3D_NAME='ChildrenOfMorta'
	game_engine=$(game_engine)
	assertEquals 'unity3d' "$game_engine"
	unset UNITY3D_NAME

	UNREALENGINE4_NAME='raji'
	game_engine=$(game_engine)
	assertEquals 'unrealengine4' "$game_engine"
	unset UNREALENGINE4_NAME

	game_engine=$(game_engine)
	assertNull "$game_engine"
}
