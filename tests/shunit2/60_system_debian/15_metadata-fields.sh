#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_debian_field_description() {
	local field_description field_description_expected script_version GAME_NAME PKG_MAIN_DESCRIPTION

	script_version='19700101.1'
	GAME_NAME='Alpha Centauri'
	PKG_MAIN_DESCRIPTION='French localization'
	field_description=$(debian_field_description 'PKG_MAIN')
	field_description_expected='Alpha Centauri - French localization
 ./play.it script version 19700101.1'
	assertEquals "$field_description_expected" "$field_description"
	unset PKG_MAIN_DESCRIPTION

	field_description=$(debian_field_description 'PKG_MAIN')
	field_description_expected='Alpha Centauri
 ./play.it script version 19700101.1'
	assertEquals "$field_description_expected" "$field_description"
}
