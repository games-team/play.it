#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_debian_script_postinst() {
	local script_postinst script_postinst_expected PKG_MAIN_POSTINST_RUN PKG_MAIN_POSTINST_WARNINGS

	PKG_MAIN_POSTINST_RUN='# Link common files shared by the games series
ln --symbolic /usr/share/games/heroes-chronicles/data /usr/share/games/heroes-chronicles-3
ln --symbolic /usr/share/games/heroes-chronicles/mp3 /usr/share/games/heroes-chronicles-3'
	script_postinst=$(debian_script_postinst 'PKG_MAIN')
	script_postinst_expected='#!/bin/sh
set -o errexit

# Link common files shared by the games series
ln --symbolic /usr/share/games/heroes-chronicles/data /usr/share/games/heroes-chronicles-3
ln --symbolic /usr/share/games/heroes-chronicles/mp3 /usr/share/games/heroes-chronicles-3

exit 0'
	assertEquals "$script_postinst_expected" "$script_postinst"
	unset PKG_MAIN_POSTINST_RUN

	PKG_MAIN_POSTINST_WARNINGS='You may need to generate the ja_JP.UTF-8 locale for the configuration program to run
You need a MIDI synthetiser for music to play in the game (you can use timidity++ or fluidsynth if you don’t have a hardware synthetiser)'
	script_postinst=$(debian_script_postinst 'PKG_MAIN')
	script_postinst_expected='#!/bin/sh
set -o errexit

printf "Warning: %s\n" "You may need to generate the ja_JP.UTF-8 locale for the configuration program to run"
printf "Warning: %s\n" "You need a MIDI synthetiser for music to play in the game (you can use timidity++ or fluidsynth if you don’t have a hardware synthetiser)"

exit 0'
	assertEquals "$script_postinst_expected" "$script_postinst"
}

test_debian_script_prerm() {
	local script_prerm script_prerm_expected PKG_MAIN_PRERM_RUN

	PKG_MAIN_PRERM_RUN='# Delete links to common files shared by the games series
rm /usr/share/games/heroes-chronicles-3/mp3
rm /usr/share/games/heroes-chronicles-3/data'
	script_prerm=$(debian_script_prerm 'PKG_MAIN')
	script_prerm_expected='#!/bin/sh
set -o errexit

# Delete links to common files shared by the games series
rm /usr/share/games/heroes-chronicles-3/mp3
rm /usr/share/games/heroes-chronicles-3/data

exit 0'
	assertEquals "$script_prerm_expected" "$script_prerm"
}
