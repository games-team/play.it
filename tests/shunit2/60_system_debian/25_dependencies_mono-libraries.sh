#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_debian_dependency_providing_mono_library() {
	## Prevent actions requiring the creation/modification of files
	dependencies_unknown_mono_libraries_add() { return 0; }

	# Check that setting a dependency on an unkown Mono library is allowed
	assertTrue \
		'debian_dependency_providing_mono_library choked on an unkwown Mono library.' \
		'debian_dependency_providing_mono_library "Mono.unknown.dll"'

	unset -f dependencies_unknown_mono_libraries_add
}
