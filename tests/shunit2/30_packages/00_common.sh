#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_packages_generation() {
	# Check that packages_generation called without an explicit list of packages does no end up running twice
	local PLAYIT_OPTION_PACKAGE function_calls_counter
	PLAYIT_OPTION_PACKAGE='deb'
	function_calls_counter=0
	debian_packages_metadata() {
		function_calls_counter=$((function_calls_counter + 1))
		export function_calls_counter
	}
	debian_packages_build() { return 0; }
	## Prevent the console message from being mixed with shunit2 output.
	packages_generation >/dev/null
	assertEquals \
		'packages_generation called without an argument ended up running multiple times instead of only once.' \
		'1' "$function_calls_counter"
	unset -f debian_packages_metadata debian_packages_build
}

test_package_description() {
	local package_description PKG_MAIN_DESCRIPTION

	PKG_MAIN_DESCRIPTION='French localization'
	package_description=$(package_description 'PKG_MAIN')
	assertEquals 'French localization' "$package_description"

	# Line breaks should trigger an error
	PKG_MAIN_DESCRIPTION='French localization
Includes text and voices'
	assertFalse 'package_description "PKG_MAIN"'
}

test_package_postinst_actions() {
	local postinst_actions postinst_actions_expected PKG_MAIN_POSTINST_RUN

	PKG_MAIN_POSTINST_RUN="# Link common files shared by the games series
ln --symbolic '/usr/share/games/heroes-chronicles/data' '/usr/share/games/heroes-chronicles-3'
ln --symbolic '/usr/share/games/heroes-chronicles/mp3' '/usr/share/games/heroes-chronicles-3'"
	postinst_actions=$(package_postinst_actions 'PKG_MAIN')
	postinst_actions_expected="# Link common files shared by the games series
ln --symbolic '/usr/share/games/heroes-chronicles/data' '/usr/share/games/heroes-chronicles-3'
ln --symbolic '/usr/share/games/heroes-chronicles/mp3' '/usr/share/games/heroes-chronicles-3'"
	assertEquals "$postinst_actions_expected" "$PKG_MAIN_POSTINST_RUN"
}

test_package_prerm_actions() {
	local prerm_actions prerm_actions_expected PKG_MAIN_PRERM_RUN

	PKG_MAIN_PRERM_RUN="# Delete links to common files shared by the games series
rm '/usr/share/games/heroes-chronicles-3/mp3'
rm '/usr/share/games/heroes-chronicles-3/data'"
	prerm_actions=$(package_prerm_actions 'PKG_MAIN')
	prerm_actions_expected="# Delete links to common files shared by the games series
rm '/usr/share/games/heroes-chronicles-3/mp3'
rm '/usr/share/games/heroes-chronicles-3/data'"
	assertEquals "$prerm_actions_expected" "$PKG_MAIN_PRERM_RUN"
}

test_package_postinst_warnings() {
	local postinst_warnings postinst_warnings_expected PKG_MAIN_POSTINST_WARNINGS

	PKG_MAIN_POSTINST_WARNINGS='You may need to generate the ja_JP.UTF-8 locale for the configuration program to run
You need a MIDI synthetiser for music to play in the game (you can use timidity++ or fluidsynth if you don’t have a hardware synthetiser)'
	postinst_warnings=$(package_postinst_warnings 'PKG_MAIN')
	postinst_warnings_expected='You may need to generate the ja_JP.UTF-8 locale for the configuration program to run
You need a MIDI synthetiser for music to play in the game (you can use timidity++ or fluidsynth if you don’t have a hardware synthetiser)'
	assertEquals "$postinst_warnings_expected" "$postinst_warnings"
}
