#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

test_scummvm_launcher() {
	local APP_MAIN_PREFIX_TYPE

	APP_MAIN_PREFIX_TYPE='none'
	assertTrue \
		'Failed to generate a launcher for a ScummVM game not using a prefix.' \
		'scummvm_launcher "APP_MAIN"'
}
