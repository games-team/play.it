#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

setUp() {
	# Set a temporary directory to mess with real files
	TEST_TEMP_DIR=$(mktemp --directory)
	export TEST_TEMP_DIR
}

tearDown() {
	rm --force --recursive "$TEST_TEMP_DIR"
}

test_options_init_default() {
	local option_prefix
	options_init_default
	option_prefix=$(option_value 'prefix')
	assertEquals '/usr' "$option_prefix"
}

test_option_variable() {
	local option_variable
	option_variable=$(option_variable 'free-space-check')
	assertEquals 'PLAYIT_OPTION_FREE_SPACE_CHECK' "$option_variable"
}

test_option_variable_default() {
	local option_variable
	option_variable=$(option_variable_default 'free-space-check')
	assertEquals 'PLAYIT_DEFAULT_OPTION_FREE_SPACE_CHECK' "$option_variable"
}

test_option_update() {
	local PLAYIT_COMPATIBILITY_LEVEL option_value

	option_update 'prefix' '/usr/local'
	option_value=$(option_value 'prefix')
	assertEquals '/usr/local' "$option_value"
}

test_option_update_default() {
	local option_value
	option_update_default 'prefix' '/usr/local'
	option_value=$(option_value 'prefix')
	assertEquals '/usr/local' "$option_value"
}

test_option_value() {
	local option_value
	option_update 'prefix' '/usr/local'
	option_value=$(option_value 'prefix')
	assertEquals '/usr/local' "$option_value"
}

test_options_validity_check() {
	options_init_default

	# Create paths expected to be existing and writable
	option_update 'tmpdir' "${TEST_TEMP_DIR}/tmpdir"
	option_update 'output-dir' "${TEST_TEMP_DIR}output-dir"
	mkdir "${TEST_TEMP_DIR}/tmpdir" "${TEST_TEMP_DIR}output-dir"

	option_update 'checksum' 'none'
	assertTrue 'options_validity_check'
	option_update 'checksum' 'invalid'
	assertFalse 'options_validity_check'
}

test_options_compatibility_check() {
	option_update 'package' 'arch'
	option_update 'compression' 'size'
	assertTrue 'options_compatibility_check'
	option_update 'compression' 'auto'
	assertFalse 'options_compatibility_check'
}
