#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
}

setUp() {
	# Set a temporary directory to mess with real files
	TEST_TEMP_DIR=$(mktemp --directory)
	export TEST_TEMP_DIR
}

tearDown() {
	rm --force --recursive "$TEST_TEMP_DIR"
}

test_getopt_arguments_cleanup() {
	local options_string
	options_string=$(getopt_arguments_cleanup --compression=auto --output-dir debian --tmpdir=/var/tmp/play.it /home/jeux/alpha-centauri/archives/gog.com/setup_sid_meiers_alpha_centauri_2.0.2.23.exe)
	# TODO: Find why there is a leading space in getopt output.
	assertEquals " --compression 'auto' --output-dir 'debian' --tmpdir '/var/tmp/play.it' -- '/home/jeux/alpha-centauri/archives/gog.com/setup_sid_meiers_alpha_centauri_2.0.2.23.exe'" "$options_string"

	# Check error on missing mandatory option value
	assertFalse 'getopt_arguments_cleanup --output-dir'

	# Check error on invalid option name
	assertFalse 'getopt_arguments_cleanup --invalid-option'
}

test_parse_arguments() {
	local SOURCE_ARCHIVE_PATH option_overwrite option_free_space_check option_prefix
	touch "${TEST_TEMP_DIR}/some_game_archive.tar.gz"
	parse_arguments --overwrite --no-free-space-check --prefix /usr/local "${TEST_TEMP_DIR}/some_game_archive.tar.gz"

	option_overwrite=$(option_value 'overwrite')
	assertEquals 1 "$option_overwrite"
	option_free_space_check=$(option_value 'free-space-check')
	assertEquals 0 "$option_free_space_check"
	option_prefix=$(option_value 'prefix')
	assertEquals '/usr/local' "$option_prefix"
	assertEquals "${TEST_TEMP_DIR}/some_game_archive.tar.gz" "$SOURCE_ARCHIVE_PATH"
}

test_parse_arguments_default() {
	local option_overwrite option_free_space_check option_tmpdir
	parse_arguments_default --overwrite --no-free-space-check --tmpdir /var/tmp/play.it

	option_overwrite=$(option_value 'overwrite')
	assertEquals 1 "$option_overwrite"
	option_free_space_check=$(option_value 'free-space-check')
	assertEquals 0 "$option_free_space_check"
	option_tmpdir=$(option_value 'tmpdir')
	assertEquals '/var/tmp/play.it' "$option_tmpdir"
}
