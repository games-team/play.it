#!/bin/sh
set -o nounset

oneTimeSetUp() {
	# Load the ./play.it library
	. lib/libplayit2.sh
	# Set some default option values
	PLAYIT_OPTION_TMPDIR=$(mktemp --directory --dry-run)
	PLAYIT_OPTION_FREE_SPACE_CHECK=0
	export PLAYIT_OPTION_TMPDIR PLAYIT_OPTION_FREE_SPACE_CHECK
}

setUp() {
	mkdir --parents "$PLAYIT_OPTION_TMPDIR"
}

tearDown() {
	rm --force --recursive "$PLAYIT_OPTION_TMPDIR"
}

test_init_working_directory() {
	local GAME_ID PLAYIT_WORKDIR
	GAME_ID='some-game'

	init_working_directory

	assertTrue "test -n '$PLAYIT_WORKDIR'"
	assertTrue "test -d '$PLAYIT_WORKDIR'"
}
